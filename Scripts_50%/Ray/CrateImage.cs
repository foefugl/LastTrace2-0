﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateImage : MonoBehaviour {

    public GameObject copyGameObject;//要被複製的物件
    public GameObject superGameObject;//要被放置在哪個物件底下

    private GameObject childGameObject;//被複製出來的物件

    public void Crate()
    {
        childGameObject = Instantiate(copyGameObject);
        childGameObject.transform.parent = superGameObject.transform;
        childGameObject.transform.localPosition = Vector3.zero;
    }

    public void Delete()
    {
        Destroy(childGameObject);
    }

}
