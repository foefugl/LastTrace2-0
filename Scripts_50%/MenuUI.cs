﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour {

    // Use this for initialization

    public GameObject StartButton;
    public GameObject LoadButton;
    public GameObject GameSetButton;
    public GameObject TeamButton;
    public GameObject ExitButton;
    public GameObject LogoImage;
    float count;

    public GameObject MenuPanel;

    //public GameObject MenuCanvas;

    static public bool load = false;
    static public bool start = false;
    // Use this for initialization

    void Awake()
    {
    }

    void Start()
    {
        count = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        count += Time.deltaTime;
        if (count > 1.5f)
        {
            LogoImage.GetComponent<Image>().color += new Color(0, 0, 0, 0.01f);
        }

        if (count > 2.5f)
        {
            StartButton.GetComponent<Image>().color += new Color(0, 0, 0, 0.01f);
            LoadButton.GetComponent<Image>().color += new Color(0, 0, 0, 0.01f);
            GameSetButton.GetComponent<Image>().color += new Color(0, 0, 0, 0.01f);
            TeamButton.GetComponent<Image>().color += new Color(0, 0, 0, 0.01f);
            ExitButton.GetComponent<Image>().color += new Color(0, 0, 0, 0.01f); 
        }
    }
    public void StartGame()
    {
        //start = true;
        SceneManager.LoadScene(1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    public void GameSet()
    {
       MenuPanel.SetActive(false);
        UIManager.Instance.ShowPanel("GameSetPanel");

    }
    public void LoadGame()
    {
        
    }
    public void Team()
    {
        MenuPanel.SetActive(false);
        UIManager.Instance.ShowPanel("TeamPanel");
    }

}
