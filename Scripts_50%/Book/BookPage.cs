﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookPage : MonoBehaviour {

    public Animator Album;

    public int[] bookPage;
    public GameObject PagePaper;
    public GameObject Pages;
    public GameObject Page1;
    public GameObject Page2;
    public GameObject Page3;

    public GameObject AlbumOb;
    public GameObject BookCanvasOb;
    public CanvasGroup BookCanvas;
    public CameraController CameraCon;

    public GameObject BookGameSetPanel;
    public bool PanelActive;

    public bool Timer;

    public GameObject WorldTwoPhoto;

	// Use this for initialization
	void Start () {
        PagePaper.SetActive(false);
        BookGameSetPanel.SetActive(false);
        PanelActive = false;

        BookCanvasOb.SetActive(false);
        BookCanvas.alpha = 0;
        InvokeRepeating("PageDelay", 1f, 0.1f);
        Timer = true;

        
	}
	
	// Update is called once per frame
	void Update () {
        
        /*if(Album.GetCurrentAnimatorStateInfo(0).IsName("Stay"))
        {
            InvokeRepeating("PageDelay", 1f, 0.1f);
            PagePaper.SetActive(false);
        }*/
        if (Album.GetCurrentAnimatorStateInfo(0).IsName("FlipNext")| Album.GetCurrentAnimatorStateInfo(0).IsName("FlipBack"))
        {
            CancelInvoke("PageDelay");
            PagePaper.SetActive(true);
        }
        if (Timer == false)
        {
            InvokeRepeating("PageDelay", 1f, 0.1f);
            if (BookCanvas.alpha == 1)
            {
                Timer = true;
            }
        }
        if (CameraCon.PlayerSetactive == false)
        {
            Cursor.lockState = CursorLockMode.None; Cursor.visible = true; 
            AlbumOb.SetActive(true);
            BookCanvasOb.SetActive(true);
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked; Cursor.visible = false;
            BookCanvas.alpha = 0;
            AlbumOb.SetActive(false);
            BookCanvasOb.SetActive(false);
        }
    }

    public void PageDelay()
    {
        BookCanvas.alpha += 0.05f;
    }

    public void PagePlus()
    {
        BookCanvas.alpha = 0;
        Timer = false;
        Album.SetTrigger("Next");
        if(bookPage[0]==0)
        {
            Page1.SetActive(false);
            Page2.SetActive(true);
            bookPage[0]=1;

        }
        else if (bookPage[0] == 1)
        {
            Page2.SetActive(false);
            Page3.SetActive(true);
            bookPage[0] = 2;
        }
    }

    public void PageMinus()
    {
        BookCanvas.alpha = 0;
        Timer = false;
        Album.SetTrigger("Back");
        if (bookPage[0] == 2)
        {
            Page3.SetActive(false);
            Page2.SetActive(true);
            bookPage[0] = 1;
        }
        else if(bookPage[0] == 1)
        {
            Page2.SetActive(false);
            Page1.SetActive(true);
            bookPage[0] = 0;
        }
    }

    public void OpenPanel()
    {
        if (PanelActive == false)
        {
            BookGameSetPanel.SetActive(true);
            Pages.SetActive(false);
            PanelActive = true;
        }
        else if (PanelActive == true)
        {
            BookGameSetPanel.SetActive(false);
            Pages.SetActive(true);
            PanelActive = false;
        }

    }
}
