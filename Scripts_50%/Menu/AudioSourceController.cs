﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioSourceController : MonoBehaviour
{
    public float MusicVolume;

    public AudioSource MenuBackMusic;
    public AudioSource NowBackMusic;
    public AudioSource World1BackMusic;

    public AudioSource DoorOpen;
    public AudioSource ChangeWorld;
    public AudioSource PaperFlipSound;
    public bool Now=true;


    void Awake()
    {
        MusicVolume = 0.7f;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MenuBackMusic.volume = MusicVolume;
        NowBackMusic.volume = MusicVolume;
        World1BackMusic.volume = MusicVolume;

    }

    public void DoorOpenPlay()
    {
        DoorOpen.Play();
    }

    public void ChangeWorldPlay()
    {
        ChangeWorld.Play();
    }

    public void PaperFlipPlay()
    {
        PaperFlipSound.Play();
    }

    /*public void BackMusicPlay()
    {
        if (SceneManager.sceneCount == 0)
        {
            MenuBackMusic.Play();
        }
        if (SceneManager.sceneCount == 1 & Now == true)
        {
            NowBackMusic.Play();
            World1BackMusic.Stop();
        }
        if (SceneManager.sceneCount == 1 & Now == false)
        {
            World1BackMusic.Play();
            NowBackMusic.Stop();
        }
    }*/
}
