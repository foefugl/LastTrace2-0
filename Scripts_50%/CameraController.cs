﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Player;
    public Camera PlayerCamera;
    public Camera PlayerCamera2;
    public Camera BookCamera;

    //public ChangeWorld _changeworld;
    public GameControl _GameControl;
    public AudioSourceController _AudioSourceController;

    public bool PlayerSetactive;
    public bool MenuLuck;

    public bool DecideChangeWorld;

    // Start is called before the first frame update
    void Start()
    {
        PlayerSetactive = true;
        DecideChangeWorld = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (PlayerSetactive == false)
            {
                Player.SetActive(true);
                PlayerSetactive = true;
            }
            else if(PlayerSetactive == true)
            {
                _AudioSourceController.PaperFlipPlay();
                Player.SetActive(false);
                PlayerSetactive =false;
            }
        }
    }

    public void ClickButton()
    {
        Player.SetActive(true);
        PlayerSetactive = true;
        if (!DecideChangeWorld)
        {
            _GameControl._ChangeWorldAnimator.SetTrigger("OutWorld1");
            DecideChangeWorld = true;
            _AudioSourceController.ChangeWorld.Play();
            _AudioSourceController.World1BackMusic.Play();
            _AudioSourceController.NowBackMusic.Stop();

        }
        else
        {
            _GameControl._ChangeWorldAnimator.SetTrigger("BackNow");
            DecideChangeWorld = false;
            _AudioSourceController.ChangeWorld.Play();
            _AudioSourceController.NowBackMusic.Play();
            _AudioSourceController.World1BackMusic.Stop();
        }
    }

    public void EndGame()
    {
  
        Player.SetActive(true);
        PlayerSetactive = true;
        Cursor.lockState = CursorLockMode.None; Cursor.visible = true;
        _GameControl._EndGame.SetTrigger("EndGame");

    }
}
