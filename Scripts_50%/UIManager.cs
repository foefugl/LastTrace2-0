﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager> {

    private string UI_GAMEPANEL_ROOT = "Prefabs/GamePanel/";
    public GameObject m_CanvasRoot;
    public Dictionary<string, GameObject> m_PanelList = new Dictionary<string, GameObject>();

    private bool CheckCanvasRootlsNull()
    {
        if (m_CanvasRoot == null)
        {
            Debug.LogError("m_CanvasRoot is null");
            return true;
        }
        else
        {
            return false;
        }  
    }

    private bool IsPanelLive(string name)
    {
        return m_PanelList.ContainsKey(name);
    }

    public GameObject ShowPanel(string name)
    {
        if (CheckCanvasRootlsNull())
        {
            return null;
        }
        
        if (IsPanelLive(name))
        {
            Debug.LogErrorFormat("[{0}] is Showing");
            return null;
        }

        GameObject loadGo = Utility.AssetRelate.ResourcesLoadCheckNull<GameObject>(UI_GAMEPANEL_ROOT + name);
        if (loadGo == null)
        {
            return null;
        }

        GameObject panel = Utility.GameObjectRelate.InstantiateGameObject(m_CanvasRoot, loadGo);
        panel.name = name;

        m_PanelList.Add(name , panel);

        return panel;
            
    }

    public void TogglePanel(string name)
    {
        if (IsPanelLive(name))
        {
            if (m_PanelList[name] != null)
            {
                m_PanelList[name].SetActive(true);
            }
        }
        else
        {
            Debug.LogErrorFormat("Not found");
        }
    }
    public void ClosePanel(string name)
    {
        if (IsPanelLive(name))
        {
            if (m_PanelList[name] != null)
            {
                Object.Destroy(m_PanelList[name]);
            }

            m_PanelList.Remove(name);
        }
        else
        {
            Debug.LogErrorFormat("Not found");
        }

    }

    public void CloseAllPanel()
    {
        foreach (KeyValuePair<string, GameObject> item in m_PanelList)
        {
            if (item.Value != null)
            {
                Object.Destroy(item.Value);
            }
        }
        m_PanelList.Clear();
    }

    public Vector2 GetCanvasSize()
    {
        if (CheckCanvasRootlsNull())
        {
            return Vector2.one*-1;
        }
        RectTransform trans = m_CanvasRoot.transform as RectTransform;
        return trans.sizeDelta;
    }
	// Use this for initialization
}
