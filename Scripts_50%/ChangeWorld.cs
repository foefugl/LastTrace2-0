﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWorld : MonoBehaviour
{
    GameObject _Camera;
    GameObject _NowPoint;
    GameObject _Player;
    GameObject _ChangeWorld1;
    
    public float speed=1;
    float firstSpeed;

    public bool _DecideChangeWorld;

    // Start is called before the first frame update
    void Start()
    {
        
        _Camera = GameObject.Find("Player Camera");
        _NowPoint = GameObject.Find("NowPoint");
        _Player = GameObject.Find("Player");
        _ChangeWorld1 = GameObject.Find("ChangeWorld1");

        _DecideChangeWorld = false;

        //firstSpeed = Vector3.Distance(_ChangeWorld.transform.position, _Camera.transform.position) * speed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeWorld1()
    {
        _NowPoint.transform.position = _Player.transform.position;
        _NowPoint.transform.rotation = _Player.transform.rotation;
        _Player.transform.position = _ChangeWorld1.transform.position;
        _Player.transform.rotation = _ChangeWorld1.transform.rotation;
    }

    public void BackNow()
    {
        _Player.transform.position = _NowPoint.transform.position;
        _Player.transform.rotation = _NowPoint.transform.rotation;
    }

}
