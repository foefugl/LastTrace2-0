﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabDoor : MonoBehaviour
{
    public AudioSource Open_AudioSource;
    public AudioSource Close_AudioSource;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayOpen()
    {
        Open_AudioSource.Play();
    }

    public void CloseOpen()
    {
        Close_AudioSource.Play();
    }
}
