﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSetPanel : MonoBehaviour {

    public GameObject UIController;
    public MenuUI menuUI;

    public GameObject AudioSourceController;
    public AudioSourceController ADSController;
    public Slider MusicSlider;
    //public Animator MenuAnimator;

    // Use this for initialization
    void Start () {
		UIController = GameObject.Find("MenuController");
        menuUI = UIController.GetComponent<MenuUI>();
        AudioSourceController = GameObject.Find("AudioController");
        ADSController = AudioSourceController.GetComponent<AudioSourceController>();
        
    }
	
	// Update is called once per frame
	void Update () {
        ADSController.MusicVolume = MusicSlider.value;
	}

    public void GameSetToMenu()
    {
        menuUI.MenuPanel.SetActive(true);
        UIManager.Instance.ClosePanel("GameSetPanel");
    }
}
