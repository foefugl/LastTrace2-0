﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RayTest : MonoBehaviour {

    public GameObject Hand;
    public Image HandImage;
    public SpriteRenderer HandImageRenderer;
    public Transform HandTransform;
    public RectTransform HandImageTransform;

    //public CrateImage crateImage;
    public bool HaveImage;

    public GameObject Target;
    public Transform TargetPosition;

    public GameObject Player;
    public Transform PlayerPosition;

    public Hitter1 _Hitter1;

    // Use this for initialization
    void Start () {

        Player = GameObject.FindGameObjectWithTag("MainCamera");
        PlayerPosition = GameObject.FindGameObjectWithTag("MainCamera").transform;

        HaveImage = false;
        Hand.SetActive(false);

    }

    // Update is called once per frame
    void Update() {

        //showImage();
        HandImage.sprite = HandImageRenderer.sprite;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0)/*Input.mousePosition*/);
        RaycastHit hit;
        LayerMask InteractiveObject = LayerMask.GetMask("InteractiveObject");

        if (Physics.Raycast(ray, out hit,100,InteractiveObject))
        {
            Debug.DrawLine(Camera.main.transform.position, hit.transform.position, Color.red, 0.1f, true);
            Debug.Log(hit.transform.name);
            if (hit.transform.name == "ALBUM")
            {
                Hand.SetActive(true);
                showImage();
                HaveImage = true;
                Target = _Hitter1._GameObject;
            }
            else
            {
                Hand.SetActive(false);
                HaveImage = false;
            }
            
            //showImage();
            //Hand.SetActive(true);
            /*if (HaveImage == false)
            {
                showImage();
                Hand.SetActive(true);
                //crateImage.Crate();
                HaveImage = true;
            }
            HandImageTransform.position = Input.mousePosition;

            //Target = GameObject.
            Debug.DrawLine(Camera.main.transform.position, hit.transform.position, Color.red, 0.1f, true);
            Debug.Log(hit.transform.name);
            Debug.Log(Input.mousePosition);
            Debug.Log(HandImageTransform.position);*/
        }
       /*else
        {
            Hand.SetActive(false);
            //crateImage.Delete();
            HaveImage = false;
        }*/
       
    }

    public void showImage()
    {
        float Distance = (PlayerPosition.position - TargetPosition.position).magnitude;
        if (Distance <= 10)
        {
            var imageColor = HandImage.color;
            imageColor.a = 1f - Distance / 10;
            HandImage.color = imageColor;
        }
        else
        {
            var imageColor = HandImage.color;
            imageColor.a = 0f;
            HandImage.color = imageColor;
        }
        Debug.Log(Distance);

    }

    public void SetInteractiveObjectOb()
    {
        //Target = 
    }

    public void SetInteractiveObjectTransform()
    {
    }

}
