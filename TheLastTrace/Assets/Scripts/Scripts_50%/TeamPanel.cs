﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TeamPanel : MonoBehaviour {

    public GameObject UIController;
    public MenuUI menuUI;

	// Use this for initialization
	void Start () {

        UIController = GameObject.Find("MenuController");
        menuUI = UIController.GetComponent<MenuUI>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void TeamToMenu()
    {
        menuUI.MenuPanel.SetActive(true);
        UIManager.Instance.ClosePanel("TeamPanel");
    }
}
