﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockController : MonoBehaviour
{
    public GameObject _past_shelf;
    public GameObject _box;
    public GameObject _bookroom_door;
    public GameObject _photo;

    //public GameObject _book_UI;
    public GameObject _bookCamera;


    // Start is called before the first frame update
    void Start()
    {
        //_book_UI.SetActive(false);
        _bookCamera.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void past_shelf_on()
    {
        _past_shelf.layer = 0;
    }

    public void box_on()
    {
        _box.layer = 0;
    }

    public void bookroom_door_on()
    {
        _bookroom_door.layer = 0;
    }

    public void photo()
    {
        _photo.layer = 0;
    }

    public void BookUIOn()
    {
        //_book_UI.SetActive(true);
        _bookCamera.SetActive(true);
    }
}
