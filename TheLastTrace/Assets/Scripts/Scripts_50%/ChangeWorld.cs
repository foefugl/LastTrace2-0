﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeWorld : MonoBehaviour
{
    public GameObject _Camera;
    //GameObject _NowPoint;
    public GameObject _Player;
    //GameObject _ChangeWorld1;
    
    public float speed=1;
    float firstSpeed;

    public bool _DecideChangeWorld;

    // Start is called before the first frame update
    void Start()
    {
        
        
        //_NowPoint = GameObject.Find("NowPoint");
        
        //_ChangeWorld1 = GameObject.Find("ChangeWorld1");

        _DecideChangeWorld = false;

        //firstSpeed = Vector3.Distance(_ChangeWorld.transform.position, _Camera.transform.position) * speed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeHouseA()
    {
        SceneManager.LoadScene(0);
        //DontDestroyOnLoad(_Player);
    }

    public void ChangeHouseB()
    {
        SceneManager.LoadScene(1);
        //DontDestroyOnLoad(_Player);
    }

    public void ChangeHouseC()
    {
        SceneManager.LoadScene(2);
        //DontDestroyOnLoad(_Player);
    }

}
