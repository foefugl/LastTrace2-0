﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitter1 : MonoBehaviour
{
   public GameObject _GameObject;
    //public GameControl _GameControl;
    public GameObject _ChangeWorld;
    public Animator _ChangeWorldAnimator;
    public GameObject _WeaponSystemobj;
    public WeaponSystem _WeaponSystem;
    public LockController _LockController;
    public CameraController _CameraController;
    public BookPage _BookPage;

    public GameObject _BookroomBooks;

    public GameObject _RayController;
    public RayTest _UIRay;
    public GameObject _AudioController;
    public AudioSourceController _AudioSourceController;

    public Animator _bookroomdoor;
    

    public GameObject _Box;
    public GameObject _PutBox;

    public bool _CanTakeBook = false;


    // Start is called before the first frame update
    void Start()
    {
        _GameObject = this.gameObject;
        _ChangeWorld = GameObject.Find("ChangeWorld");
        _ChangeWorldAnimator = _ChangeWorld.GetComponent<Animator>();
        _WeaponSystemobj = GameObject.Find("Weapon");
        _WeaponSystem = _WeaponSystemobj.GetComponent<WeaponSystem>();
        _RayController = GameObject.Find("RayController");
        _UIRay = _RayController.GetComponent<RayTest>();
        _AudioController = GameObject.Find("AudioController");
        _AudioSourceController = _AudioController.GetComponent<AudioSourceController>();

    }

    // Update is called once per frame
    void Update()
    {
 
    }

    void HitByRaycast() //被射線打到時會進入此方法
    {

        if (_GameObject.name == "ALBUM" && _CanTakeBook == false)
        {
            //RayHitter();
            //_UIRay.TargetPosition.transform.position = _GameObject.transform.position;
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _AudioSourceController.ChangeWorld.Play();
                _AudioSourceController.World1BackMusic.Play();
                _AudioSourceController.NowBackMusic.Stop();
                _ChangeWorldAnimator.SetTrigger("OutWorld1");
                //_GameControl.Back();
                _CanTakeBook = true;
                //Destroy(this.gameObject);
            }

        }
        else if (_GameObject.name == "ALBUM" && _CanTakeBook == true)
        {
            //RayHitter();
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _LockController.BookUIOn();
                Destroy(this.gameObject);
            }
        }

        if (_GameObject.name == "album_past") 
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _AudioSourceController.ChangeWorld.Play();
                _AudioSourceController.NowBackMusic.Play();
                _AudioSourceController.World1BackMusic.Stop();
                _ChangeWorldAnimator.SetTrigger("BackNow");
                //_GameControl.Back();
                _LockController.past_shelf_on();
                _bookroomdoor.SetTrigger("Opendoor");
                Destroy(this.gameObject);
            }

        }

        if (_GameObject.name == "key") 
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                //_WeaponSystem._GetBKey();
                _LockController.bookroom_door_on();
                Destroy(this.gameObject);
            }

        }

        if (_GameObject.name == "Box1")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _Box.SetActive(true);               
                Destroy(this.gameObject);
            }

        }

        if (_GameObject.name == "PutBoxPoint")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _bookroomdoor.SetTrigger("Opendoor");
                _BookroomBooks.SetActive(true);
                _Box.SetActive(false);
                _PutBox.SetActive(true);
            }

        }

        if (_GameObject.name == "door")
        {
            
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _AudioSourceController.DoorOpen.Play();
                _bookroomdoor.SetTrigger("Opendoor");
                _LockController.box_on();
            }

        }

        if (_GameObject.name == "photo01")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _AudioSourceController.PaperFlipPlay();
                _CameraController.Player.SetActive(false);
                _CameraController.PlayerSetactive = false;
                _BookPage.BookCanvas.alpha = 0;
                _BookPage.WorldTwoPhoto.SetActive(true);
                Destroy(this.gameObject);
            }

        }

    }

    public void RayHitter()
    {
        _UIRay.Hand.SetActive(true);
        _UIRay.showImage();
        _UIRay.HaveImage = true;
        _UIRay.Target = this.gameObject;
    }
}
