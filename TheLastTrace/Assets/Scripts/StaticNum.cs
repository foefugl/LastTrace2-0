﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class  StaticNum
    
{
    public static bool PowerUI = false;

    public static bool IdCardDoorLock = false;    //消毒室是否鎖上

    public static bool BookIsTake = false;
    public static bool BookcaseIsMove = false;
    public static bool Lightcontroller = true;    //控制大廳及警衛室燈光
    public static bool TakeIDCard = false;        //是否取得ID卡
    public static bool TakeReadCard = false;      //是否取得記憶卡

    public static bool ReadCard = false;          //增加或刪除記憶卡
    public static bool SomeThingLifted = false;   //是否有抬起物品


    //哪種機器手臂拿在手中
    public static bool LiftUprb_hand01 = false;
    public static bool LiftUprb_hand01_2 = false;


    public static bool LiftUprb_hand02 = false;
    public static bool LiftUprb_hand02_1 = false;
    public static bool LiftUprb_hand02_2 = false;
    public static bool LiftUprb_hand03 = false;
    public static bool LiftUprb_hand04 = false;
    public static bool LiftUprb_hand05 = false;
    public static bool LiftUprb_hand06 = false;

    //機器手臂組到哪裡
    public static int rb_handMode = 1;
}
