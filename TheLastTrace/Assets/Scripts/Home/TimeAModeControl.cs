﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimeAModeControl : MonoBehaviour
{
    [Header("TimeLine物件")]
    public GameObject paperbox_directorOBJ;
    public PlayableDirector paperbox_director;
    public GameObject balcony_directorOBJ;
    public PlayableDirector balcony_director;

    [Header("Photo物件")]
    public SkinnedMeshRenderer AlbumMaterial1;
    public SkinnedMeshRenderer AlbumMaterial2;
    public Material Photo1_3CantUse;
    public Material Photo2Material;
    public Material Photo3Material;
    public Material Photo4Material;
    public Material Photo5Material;

    [Header("3DUI物件")]
    public GameObject UI1;
    public GameObject UI2;
    public GameObject UI3;
    public GameObject UI4;
    public GameObject UI5;
    public GameObject UI6;



    [Header("Mode1的物件")]
    public GameObject Mode1Point;
    public GameObject Player;
    public GameObject BedroomAlbum;

    [Header("Mode2的物件")]
    public GameObject Mode2Point;
    public GameObject past_shelf_NeedMove;
    public GameObject past_shelf;

    [Header("Mode3的物件")]
    public GameObject Mode3Point;
    public Animator BookRoomDoor;
    public GameObject Model;
    public GameObject ModelOnTable;

    [Header("Mode4的物件")]
    public GameObject Mode4Point;
    public GameObject BalconydoorAfter;
    public GameObject Balconydoor;

    [Header("Mode5的物件")]
    public GameObject FlowerAfter;
    public GameObject Flower;

    // Start is called before the first frame update
    void Start()
    {
        if(AlbumStatic.RoomAMode3&&!AlbumStatic.paperbox_director)
        {
            paperbox_directorOBJ.SetActive(true);
            paperbox_director.Play();
            AlbumStatic.paperbox_director = true;
        }

        if (AlbumStatic.RoomAMode4 && !AlbumStatic.balcony_director)
        {
            balcony_directorOBJ.SetActive(true);
            balcony_director.Play();
            AlbumStatic.balcony_director = true;
        }



        if (AlbumStatic.RoomAMode1)
        {
            UI1.SetActive(true);
        }
        else
        {
            UI1.SetActive(false);
        }




        if (AlbumStatic.Flower)
        {
            Flower.SetActive(false);
            FlowerAfter.SetActive(true);
        }

        if (AlbumStatic.Balconydoor)
        {
            BalconydoorAfter.SetActive(true);
            Balconydoor.SetActive(false);
        }

        if (AlbumStatic.ModelIsOnTable)
        {
            Model.SetActive(false);
            ModelOnTable.SetActive(true);
        }

        if(AlbumStatic.ReadingRoom)
        {
            BookRoomDoor.SetTrigger("Open");
        }

        if (!AlbumStatic.BookShelf)
        {
            past_shelf_NeedMove.SetActive(true);
            past_shelf.SetActive(false);
        }
        else
        {
            past_shelf_NeedMove.SetActive(false);
            past_shelf.SetActive(true);
        }

        if (AlbumStatic.Album)
        {
            BedroomAlbum.SetActive(false);
        }

        if (AlbumStatic.RoomAMode1)
        {
            Player.transform.position = Mode1Point.transform.position;
            Player.transform.rotation = Mode1Point.transform.rotation;

        }


        if (AlbumStatic.RoomAMode2)
        {
            Player.transform.position = Mode2Point.transform.position;
            Player.transform.rotation = Mode2Point.transform.rotation;

        }

        if (AlbumStatic.RoomAMode3)
        {
            Player.transform.position = Mode3Point.transform.position;
            Player.transform.rotation = Mode3Point.transform.rotation;

        }

        if (AlbumStatic.RoomAMode4)
        {
            Player.transform.position = Mode4Point.transform.position;
            Player.transform.rotation = Mode4Point.transform.rotation;

        }

        if (AlbumStatic.RoomAMode5)
        {
            Player.transform.position = Mode4Point.transform.position;
            Player.transform.rotation = Mode4Point.transform.rotation;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (AlbumStatic.RoomAMode2 && !AlbumStatic.BookShelf)
        {
            UI2.SetActive(true);
        }
        else
        {
            UI2.SetActive(false);
        }

        if (!AlbumStatic.ModelIsOnHand&&!AlbumStatic.ModelIsOnTable&&AlbumStatic.RoomAMode3)
        {
            UI3.SetActive(true);
        }
        else
        {
            UI3.SetActive(false);
        }

        if (AlbumStatic.ModelIsOnHand)
        {
            UI4.SetActive(true);
        }
        else
        {
            UI4.SetActive(false);
        }

        if (AlbumStatic.RoomAMode4&&!AlbumStatic.Balconydoor)
        {
            UI5.SetActive(true);
        }
        else
        {
            UI5.SetActive(false);
        }

        if (AlbumStatic.RoomAMode5 && !AlbumStatic.Flower)
        {
            UI6.SetActive(true);
        }
        else
        {
            UI6.SetActive(false);
        }


        if (AlbumStatic.photo2)
        {
            AlbumMaterial1.material = Photo2Material;
        }
        if (AlbumStatic.photo3)
        {
            AlbumMaterial1.material = Photo3Material;
        }
        if (AlbumStatic.photo4)
        {
            AlbumMaterial2.material = Photo4Material;
            AlbumMaterial1.material = Photo1_3CantUse;
        }
        if (AlbumStatic.photo5)
        {
            AlbumMaterial2.material = Photo5Material;
            AlbumMaterial1.material = Photo1_3CantUse;
        }


    }
}
