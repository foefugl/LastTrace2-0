﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class ChangeTimeHomeC : MonoBehaviour
{
    public GameObject Photo;
    public Animator Animator;

    //GameObject _NowPoint;

    //GameObject _ChangeWorld1;





    // Start is called before the first frame update
    void Start()
    {
        Photo = this.gameObject;




    }

    // Update is called once per frame
    void Update()
    {




    }
    private void OnMouseDown()
    {
        if(Photo.name== "OnTableAlbum")
        {
            Invoke("ChangeHouseA", 1);
            Animator.SetTrigger("left");

        }

        if (Photo.name == "p1" && AlbumStatic.photo1)
        {
            Invoke("ChangeHouseA", 1);
            Animator.SetTrigger("left");
            
        }
        if (Photo.name == "p2" && AlbumStatic.photo2)
        {
            Invoke("ChangeHouseA", 1);
            Animator.SetTrigger("left");
            
        }
        if (Photo.name == "p3"&& AlbumStatic.photo3)
        {           
            Invoke("ChangeHouseA", 1);
            Animator.SetTrigger("left");
            
        }
        if (Photo.name == "p4" && AlbumStatic.photo4)
        {
            Animator.SetTrigger("left");
            Invoke("ChangeHouseB", 1);
            
        }
        if (Photo.name == "p5" && AlbumStatic.photo5)
        {
            Animator.SetTrigger("left");
            Invoke("ChangeLab", 1);
            
        }
    }
    public void Leave()
    {
        Application.Quit();
    }

    public void ChangeHouseA()
    {
        SceneManager.LoadScene(1);

    }

    public void ChangeHouseB()
    {
        SceneManager.LoadScene(2);

    }

    public void ChangeHouseC()
    {
        SceneManager.LoadScene(3);

    }

    public void ChangeLab()
    {
        SceneManager.LoadScene(4);
    }
}
