﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AlbumStatic
{
    //玫瑰花是否拿在手中
    public static bool RoseOnHand = false;

    //TimeA的TimeLine是否撥放
    public static bool paperbox_director = false;
    public static bool balcony_director = false;

    //提示是否關閉
    public static bool Text = false;

    //照片是否撿取
    public static bool photo1=false;
    public static bool photo2=false;
    public static bool photo3=false;
    public static bool photo4=false;
    public static bool photo5=false;

    //音效是否撥放
    public static bool ShelfAudioSource = false;
    public static bool FlowerAudioSource = false;


    //是否撿取暗房鑰匙
    public static bool BlackRoomKey = false;
    public static bool BlackRoomDoor = false;

    //密碼鎖是否被打開
    public static bool PassLock = false;

    //花盆是否移動
    public static bool Flower = false;

    //相簿是否撿取
    public static bool Album = false;

    //鑰匙是否撿起
    public static bool Key = false;

    //書櫃是否移動
    public static bool BookShelf=false;

    //書房門是否打開
    public static bool ReadingRoom = false;

    //書房玻璃門是否打開
    public static bool Balconydoor = false;

    //是否有拿起箱子
    public static bool ModelIsOnHand = false;

    //箱子是否放在桌上
    public static bool ModelIsOnTable = false;


    //RoomA的時間模式
    public static bool RoomAMode1 = true;
    public static bool RoomAMode2 = false;
    public static bool RoomAMode3 = false;
    public static bool RoomAMode4 = false;
    public static bool RoomAMode5 = false;

    //RoomB的時間模式
    public static bool RoomBMode1 = true;
    public static bool RoomBMode2 = false;
    public static bool RoomBMode3 = false;

    //RoomC的時間模式
    public static bool RoomCMode1 = true;
    public static bool RoomCMode2 = false;
    public static bool RoomCMode3 = false;
    public static bool RoomCMode4 = false;
    public static bool RoomCMode5 = false;
    public static bool RoomCMode6 = false;
    public static bool RoomCMode7 = false;

    public static bool Lab = false;






}
