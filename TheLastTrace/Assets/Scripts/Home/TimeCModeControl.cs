﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeCModeControl : MonoBehaviour
{
    [Header("提示組件")]
    public GameObject Text;

    [Header("場景音效物件")]
    public AudioSource ShelfAudioSource;
    public AudioSource FlowerAudioSource;
    public GameObject BGM1;
    public GameObject BGM2;

    [Header("Photo物件")]
    public SkinnedMeshRenderer AlbumMaterial1;
    public SkinnedMeshRenderer AlbumMaterial2;
    public Material Photo1_3CantUse;
    public Material Photo2Material;
    public Material Photo3Material;
    public Material Photo4Material;
    public Material Photo5Material;

    [Header("3DUI物件")]
    public GameObject UI1;
    public GameObject UI2;
    public GameObject UI3;
    public GameObject UI4;
    public GameObject UI5;


    [Header("Mode1的物件")]
    public GameObject OnTableAlbum;
    public GameObject _Player;
    public GameObject SitPoint;
    public Rigidbody PlayerRigidbody;
    public Collider PlayerCollider;
    public FirstPersonAIO _FirstPersonAIO;

    [Header("Mode2的物件")]
    public GameObject Mode2Point;
    public GameObject Photo2;

    [Header("Mode3的物件")]
    public GameObject shelf;
    public GameObject shelf_After;
    public GameObject Mode3Point;

    [Header("Mode4的物件")]
    public Animator BookRoomDoor;

    public GameObject Photo3;
    public GameObject Mode4Point;

    [Header("Mode5的物件")]
    public GameObject Balcony_door;
    public GameObject Balcony_doorAfter;
    public GameObject FlowerAfter;
    public GameObject Flower;
    public GameObject Mode5Point;

    [Header("Mode6的物件")]
    public GameObject Photo4;

    [Header("Mode7的物件")]
    public GameObject Mode7Point;
    public GameObject Rose;
    public GameObject RoseUI;
    public GameObject timeC_endgameOpen;



    // Start is called before the first frame update
    void Start()
    {



        if(AlbumStatic.RoomCMode7)
        {
            Rose.SetActive(true);
            RoseUI.SetActive(true);
            BGM1.SetActive(false);
            BGM2.SetActive(true);
            timeC_endgameOpen.SetActive(true);
        }
        else
        {
            Rose.SetActive(false);
            RoseUI.SetActive(false);
            BGM1.SetActive(true);
            BGM2.SetActive(false);
            
        }


        if (AlbumStatic.RoomCMode2 && !AlbumStatic.ShelfAudioSource)
        {
            ShelfAudioSource.Play();
            AlbumStatic.ShelfAudioSource = true;
        }

        if (AlbumStatic.RoomCMode5 && !AlbumStatic.FlowerAudioSource)
        {
            FlowerAudioSource.Play();
            AlbumStatic.FlowerAudioSource = true;
        }





        if (AlbumStatic.photo4||AlbumStatic.photo5)
        {
            Photo4.SetActive(false);
        }



        if (AlbumStatic.Flower)
        {
            Flower.SetActive(false);
            FlowerAfter.SetActive(true);
        }

        if (AlbumStatic.Balconydoor)
        {
            Balcony_door.SetActive(false);
            Balcony_doorAfter.SetActive(true);
        }

        if(AlbumStatic.ReadingRoom)
        {
            BookRoomDoor.SetTrigger("Open");
        }

        if (AlbumStatic.ModelIsOnTable)
        {           
            if(!AlbumStatic.photo3)
            Photo3.SetActive(true);
        }

        if (AlbumStatic.photo3 || AlbumStatic.photo4 || AlbumStatic.photo5)
        {
            Photo3.SetActive(false);
        }



        if (!AlbumStatic.BookShelf)
        {
            shelf.SetActive(true);
            shelf_After.SetActive(false);
        }
        else
        {
            shelf.SetActive(false);
            shelf_After.SetActive(true);
        }



        //Mode1的時候做這些事
        if (AlbumStatic.RoomCMode1)
        {
            PlayerCollider.enabled = false;
            PlayerRigidbody.useGravity = false;
            _FirstPersonAIO.playerCanMove = false;
            _FirstPersonAIO.rotationRange.y = 180;
            _Player.transform.position = SitPoint.transform.position;
            _Player.transform.rotation = SitPoint.transform.rotation;
            OnTableAlbum.SetActive(true);
            UI1.SetActive(true);
        }
        else
        {
            UI1.SetActive(false);
            OnTableAlbum.SetActive(false);
        }

        if(AlbumStatic.RoomCMode2)
        {
            _Player.transform.position = Mode2Point.transform.position;
            _Player.transform.rotation = Mode2Point.transform.rotation;
        }

        if (AlbumStatic.RoomCMode3)
        {
            
            if (!AlbumStatic.photo2)
            {
                Photo2.SetActive(true);
            }
            _Player.transform.position = Mode3Point.transform.position;
            _Player.transform.rotation = Mode3Point.transform.rotation;
        }
        else
        {
            
        }

        if (AlbumStatic.RoomCMode4)
        {


            _Player.transform.position = Mode4Point.transform.position;
            _Player.transform.rotation = Mode4Point.transform.rotation;
        }


        if (AlbumStatic.RoomCMode5)
        {
            AlbumStatic.RoomAMode5 = true;
            AlbumStatic.RoomAMode4 = false;

            _Player.transform.position = Mode5Point.transform.position;
            _Player.transform.rotation = Mode5Point.transform.rotation;
        }

        if (AlbumStatic.RoomCMode6)
        {


            _Player.transform.position = Mode5Point.transform.position;
            _Player.transform.rotation = Mode5Point.transform.rotation;
        }

        if (AlbumStatic.RoomCMode7)
        {


            _Player.transform.position = Mode7Point.transform.position;
            _Player.transform.rotation = Mode7Point.transform.rotation;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(AlbumStatic.RoomCMode2&&!AlbumStatic.Text)
        {
            Text.SetActive(true);
        }
        else
        {
            Text.SetActive(false);
        }


        if (AlbumStatic.photo2)
        {
            AlbumMaterial1.material = Photo2Material;
        }
        if (AlbumStatic.photo3)
        {
            AlbumMaterial1.material = Photo3Material;
        }
        if (AlbumStatic.photo4)
        {
            AlbumMaterial2.material = Photo4Material;
            AlbumMaterial1.material = Photo1_3CantUse;
        }
        if (AlbumStatic.photo5)
        {
            AlbumMaterial2.material = Photo5Material;
            AlbumMaterial1.material = Photo1_3CantUse;
        }


        if (!AlbumStatic.photo2&&AlbumStatic.RoomCMode3)
        {
            UI2.SetActive(true);
        }
        else
        {
            UI2.SetActive(false);
        }

        if (!AlbumStatic.photo3 && AlbumStatic.RoomCMode4)
        {
            UI3.SetActive(true);
        }
        else
        {
            UI3.SetActive(false);
        }

        if (!AlbumStatic.PassLock && AlbumStatic.RoomCMode6)
        {
            UI4.SetActive(true);
        }
        else
        {
            UI4.SetActive(false);
        }



    }
 
}

