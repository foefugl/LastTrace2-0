﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseAlbum : MonoBehaviour
{
    public GameObject Album;
    public OpenAlbum _OpenAlbum;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void _CloseAlbum()
    {
        Album.SetActive(false);
        _OpenAlbum.LockOpen = true;
    }
}
