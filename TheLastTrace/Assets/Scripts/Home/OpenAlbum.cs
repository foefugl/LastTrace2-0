﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OpenAlbum : MonoBehaviour
{
    public Animator _Animator;
    public GameObject Album;
    public bool IsOpen=false;
    public bool LockOpen = true;
    public Transform albumPoint;
    public FirstPersonAIO _FirstPersonAIO;
    public GameObject P1;
    public GameObject P2;
    public GameObject P3;
    public GameObject P4;
    public GameObject P5;
    public RayPlay _RayPlay;
    public AudioSource OpenSource;





    // Start is called before the first frame update
    void Start()
    {
        P1.SetActive(false);
        P2.SetActive(false);
        P3.SetActive(false);
        P4.SetActive(false);
        P5.SetActive(false);

        Album.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {



        if (!IsOpen&&Input.GetKeyDown(KeyCode.Space)&& LockOpen&& AlbumStatic.Album)
        {
            Album.transform.position = albumPoint.position;
            Album.SetActive(true);
            IsOpen = true;
            LockOpen = false;
            _FirstPersonAIO.enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            OpenPhoto();
            _RayPlay.enabled = false;
            OpenSource.Play();
            AlbumStatic.Text = true;


        }
        else if(IsOpen&Input.GetKeyDown(KeyCode.Space) )
        {
            _FirstPersonAIO.enabled = true;

            IsOpen = false;
            _Animator.SetTrigger("PutDown");
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            P1.SetActive(false);
            P2.SetActive(false);
            P3.SetActive(false);
            P4.SetActive(false);
            P5.SetActive(false);
            _RayPlay.enabled = true;
            
        }
    }
    void OpenPhoto()
    {
        if(AlbumStatic.photo1)
        {
            P1.SetActive(true);
        }
        if (AlbumStatic.photo2)
        {
            AlbumStatic.photo1=false;
            P1.SetActive(false);
            P2.SetActive(true);
        }
        if (AlbumStatic.photo3)
        {
            AlbumStatic.photo2 = false;
            P2.SetActive(false);
            P3.SetActive(true);
        }
        if (AlbumStatic.photo4)
        {
            AlbumStatic.photo3 = false;
            P3.SetActive(false);
            P4.SetActive(true);
        }
        if (AlbumStatic.photo5)
        {
            AlbumStatic.photo4 = false;
            P4.SetActive(false);
            P5.SetActive(true);
        }


    }

}
