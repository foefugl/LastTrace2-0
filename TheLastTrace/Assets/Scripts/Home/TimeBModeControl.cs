﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBModeControl : MonoBehaviour
{
    [Header("3DUI物件")]
    public GameObject UI1;
    public GameObject UI2;
    public GameObject UI3;

    [Header("Photo物件")]
    public SkinnedMeshRenderer AlbumMaterial2;
    public Material Photo4Material;
    public Material Photo5Material;

    [Header("Mode1的物件")]
    public GameObject BlackRoomKey;
    public GameObject BlackRoomDoor;
    public GameObject BlackRoomDoorAfter;

    // Start is called before the first frame update
    void Start()
    {
        if(AlbumStatic.BlackRoomKey)
        {
            BlackRoomKey.SetActive(false);
        }
        if(AlbumStatic.BlackRoomDoor)
        {
            BlackRoomDoor.SetActive(false);
            BlackRoomDoorAfter.SetActive(true);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(!AlbumStatic.BlackRoomKey)
        {
            UI1.SetActive(true);
        }
        else
        {
            UI1.SetActive(false);
        }

        if(AlbumStatic.BlackRoomKey&&!AlbumStatic.BlackRoomDoor)
        {
            UI2.SetActive(true);
        }
        else
        {
            UI2.SetActive(false);
        }

        if(AlbumStatic.photo5)
        {
            UI3.SetActive(false);
        }

        if (AlbumStatic.photo4)
        {
            AlbumMaterial2.material = Photo4Material;
           
        }
        if (AlbumStatic.photo5)
        {
            AlbumMaterial2.material = Photo5Material;
            
        }
    }
}
