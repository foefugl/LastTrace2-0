﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedIDCardBigDoorController : MonoBehaviour
{
    //需要ID卡的大門控制
    Animator _BigDoor;
    public Renderer Screen;
    public Material NeedIDCardOff;
    public Material NeedIDCardOffStop;
    public Material CanGo;
    public AudioSource lab_lincense;
    public AudioSource lab_big_door_open;
    // Start is called before the first frame update
    void Start()
    {
        _BigDoor = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!StaticNum.TakeIDCard)
        {
            Screen.material = NeedIDCardOffStop;
            lab_lincense.Play();
        }
        else if(StaticNum.TakeIDCard)
        {
            _BigDoor.SetTrigger("Open");
            Screen.material = CanGo;
            lab_big_door_open.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _BigDoor.SetTrigger("Close");
        Screen.material = NeedIDCardOff;
    }
}
