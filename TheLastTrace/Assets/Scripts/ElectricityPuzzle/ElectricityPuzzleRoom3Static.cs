﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ElectricityPuzzleRoom3Static
{
    public static float RoomCheckA = 0;
    public static float RoomCheckB = 0;
    public static float RoomCheckC = 0;
    public static float RoomCheckD = 0;
    public static float RoomCheckE = 0;
    public static float RoomCheckF = 0;
    public static float RoomCheckG = 0;
    public static float RoomCheckH = 0;

    public static bool RoomBoolA = false;
    public static bool RoomBoolB = false;
    public static bool RoomBoolC = false;
    public static bool RoomBoolD = false;
    public static bool RoomBoolE = false;
    public static bool RoomBoolF = false;
    public static bool RoomBoolG = false;
    public static bool RoomBoolH = false;
}
