﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityPuzzleAnimation : MonoBehaviour
{
    Animator CameraAnimator;
    public GameObject Player;
    public GameObject Game;
    public bool End = false;
    public NoPowerBigDoorController _NoPowerBigDoorController;
    public Hitter _Hitter;
    public bool CanEsc = true;
    public AudioSource _AudioSource;

    [Header("Room1按鈕")]
    public Collider Room1ui_plane1;
    public Collider Room1ui_plane2;
    public Collider Room1ui_plane3;
    public Collider Room1ui_plane4;
    public Collider Room1ui_plane5;
    public Collider Room1ui_plane6;
    public Collider Room1ui_plane7;
    public Collider Room1ui_plane8;

    [Header("Room2按鈕")]
    public Collider Room2ui_plane1;
    public Collider Room2ui_plane2;
    public Collider Room2ui_plane3;
    public Collider Room2ui_plane4;
    public Collider Room2ui_plane5;
    public Collider Room2ui_plane6;


    [Header("Room3按鈕")]
    public Collider Room3ui_plane1;
    public Collider Room3ui_plane2;
    public Collider Room3ui_plane3;
    public Collider Room3ui_plane4;
    public Collider Room3ui_plane5;

    public GameObject AllUI;




    // Start is called before the first frame update
    void Start()
    {
        CameraAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.Escape)&& CanEsc)
        {
            Player.SetActive(true);
            Game.SetActive(false);
            _Hitter.ZoomCh = 2;
            _Hitter.InLineGame = false;
            _Hitter.IsZoomIn = true;
        }*/

        if(ElectricityPuzzleRoom1Static.Room1BoolA == true && ElectricityPuzzleRoom1Static.Room1BoolB == true && ElectricityPuzzleRoom1Static.Room1BoolC == true && ElectricityPuzzleRoom1Static.Room1BoolD == true && ElectricityPuzzleRoom1Static.Room1BoolE == true && ElectricityPuzzleRoom1Static.Room1BoolF == true && ElectricityPuzzleRoom1Static.Room1BoolG == true && ElectricityPuzzleRoom1Static.Room1BoolH == true)

        {
            
            CanEsc = false;
            CameraAnimator.SetTrigger("Room1End");
            Room1ui_plane1.enabled = false;
            Room1ui_plane2.enabled = false;
            Room1ui_plane3.enabled = false;
            Room1ui_plane4.enabled = false;
            Room1ui_plane5.enabled = false;
            Room1ui_plane6.enabled = false;
            Room1ui_plane7.enabled = false;
            Room1ui_plane8.enabled = false;

        }

        if (ElectricityPuzzleRoom2Static.RoomBoolA == true && ElectricityPuzzleRoom2Static.RoomBoolB == true && ElectricityPuzzleRoom2Static.RoomBoolC == true && ElectricityPuzzleRoom2Static.RoomBoolD == true && ElectricityPuzzleRoom2Static.RoomBoolE == true && ElectricityPuzzleRoom2Static.RoomBoolF == true && ElectricityPuzzleRoom2Static.RoomBoolG == true)
        {
            
            CanEsc = false;
            CameraAnimator.SetTrigger("Room2End");
            Room2ui_plane1.enabled = false;
            Room2ui_plane2.enabled = false;
            Room2ui_plane3.enabled = false;
            Room2ui_plane4.enabled = false;
            Room2ui_plane5.enabled = false;
            Room2ui_plane6.enabled = false;
        }

        if (ElectricityPuzzleRoom3Static.RoomBoolA == true && ElectricityPuzzleRoom3Static.RoomBoolB == true && ElectricityPuzzleRoom3Static.RoomBoolC == true && ElectricityPuzzleRoom3Static.RoomBoolD == true && ElectricityPuzzleRoom3Static.RoomBoolE == true)
        {
            
            CanEsc = false;
            CameraAnimator.SetTrigger("Room3End");
            Room3ui_plane1.enabled = false;
            Room3ui_plane2.enabled = false;
            Room3ui_plane3.enabled = false;
            Room3ui_plane4.enabled = false;
            Room3ui_plane5.enabled = false;
    
        }

        if(End)
        {
            CanEsc = false;
            GameEnd();
        }

    }

    public void GameEnd()
    {
        _Hitter.ZoomCh = 2;
        _NoPowerBigDoorController.LineOn = true;
        _Hitter.InLineGame = true;
        _Hitter.CanPlay = false;
        AllUI.SetActive(true);
    }
}
