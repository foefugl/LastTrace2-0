﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityPuzzleRoom1 : MonoBehaviour
{
    public float Mode = 0;
    public LineRenderer _LineRenderer;
    public Material RadLine;
    public Material GreenLine;

    public MeshRenderer Screen;
    public Material ScreenGreenMaterial;
    public Material ScreenRedMaterial;

    // Start is called before the first frame update

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        /*if (ElectricityPuzzleRoom1Static.Room1CheckA % 2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckA = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckA % 2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckA = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckB % 2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckB = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckB % 2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckB = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckC % 2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckC = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckC % 2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckC = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckD % 2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckD = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckD % 2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckD = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckE % 2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckE = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckE % 2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckE = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckF%2 ==1)
            ElectricityPuzzleRoom1Static.Room1CheckF = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckF % 2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckF = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckG%2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckG = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckG%2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckG = 2;

        if (ElectricityPuzzleRoom1Static.Room1CheckH%2 == 1)
            ElectricityPuzzleRoom1Static.Room1CheckH = 1;
        else if (ElectricityPuzzleRoom1Static.Room1CheckH%2 == 0)
            ElectricityPuzzleRoom1Static.Room1CheckH = 2;*/

        //CheckA
        if (Mode == 1)
        {
            if (ElectricityPuzzleRoom1Static.Room1CheckA == 2)
            {
                _LineRenderer.material = GreenLine;
                ElectricityPuzzleRoom1Static.Room1BoolA = true;
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolA = false;
            }
        }

        //CheckB
        if (Mode == 2)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolA == true && ElectricityPuzzleRoom1Static.Room1CheckB == 2)
            {
                _LineRenderer.material = GreenLine;
                ElectricityPuzzleRoom1Static.Room1BoolB = true;
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolB = false;
            }
        }

        //CheckC
        if (Mode == 3)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolB == true && ElectricityPuzzleRoom1Static.Room1CheckC == 2)
            {
                _LineRenderer.material = GreenLine;
                ElectricityPuzzleRoom1Static.Room1BoolC = true;
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolC = false;
            }
        }

        //CheckD
        if (Mode == 4)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolC == true && ElectricityPuzzleRoom1Static.Room1CheckD == 2)
            
                
                {
                    _LineRenderer.material = GreenLine;
                    ElectricityPuzzleRoom1Static.Room1BoolD = true;
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom1Static.Room1BoolD = false;
                }
            
        }

        //CheckE
        if (Mode == 5)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolC == true)
            {
                if (ElectricityPuzzleRoom1Static.Room1CheckE == 2)
                {

                    _LineRenderer.material = GreenLine;
                    ElectricityPuzzleRoom1Static.Room1BoolE = true;
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom1Static.Room1BoolE = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolE = false;
            }
        }

        //CheckF
        if (Mode == 6)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolD == true || ElectricityPuzzleRoom1Static.Room1BoolG == true)
            {
                if (ElectricityPuzzleRoom1Static.Room1CheckF == 2)
                {
                    if (!ElectricityPuzzleRoom1Static.Room1BoolC)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom1Static.Room1BoolF = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom1Static.Room1BoolF = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom1Static.Room1BoolF = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolF = false;
            }
        }


        //CheckG
        if (Mode == 7)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolE == true | ElectricityPuzzleRoom1Static.Room1BoolF == true)
            {
                if (ElectricityPuzzleRoom1Static.Room1CheckG == 2)
                {
                    if (!ElectricityPuzzleRoom1Static.Room1BoolC)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom1Static.Room1BoolG = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom1Static.Room1BoolG = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom1Static.Room1BoolG = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolG = false;
            }

        }

        //CheckH
        if (Mode == 8)
        {
            if (ElectricityPuzzleRoom1Static.Room1BoolE == true || ElectricityPuzzleRoom1Static.Room1BoolG == true)
            {
                if (ElectricityPuzzleRoom1Static.Room1CheckH == 2)
                {
                    if (!ElectricityPuzzleRoom1Static.Room1BoolC)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom1Static.Room1BoolH = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom1Static.Room1BoolH = true;
                    }
                    
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom1Static.Room1BoolH = false;
                    
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom1Static.Room1BoolH = false;
                
            }
            
            if(ElectricityPuzzleRoom1Static.Room1BoolA==true&& ElectricityPuzzleRoom1Static.Room1BoolB == true&& ElectricityPuzzleRoom1Static.Room1BoolC == true&& ElectricityPuzzleRoom1Static.Room1BoolD == true&&ElectricityPuzzleRoom1Static.Room1BoolE == true&& ElectricityPuzzleRoom1Static.Room1BoolF == true&& ElectricityPuzzleRoom1Static.Room1BoolG == true&& ElectricityPuzzleRoom1Static.Room1BoolH == true)
            {
                Screen.material = ScreenGreenMaterial;
            }
            else
            {
                Screen.material = ScreenRedMaterial;
            }
        }

    }
    void OnTriggerEnter(Collider other)
    {
        //LineA
        if (Mode == 1)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane2")
                ElectricityPuzzleRoom1Static.Room1CheckA++;
        }

        //LineB
        if (Mode == 2)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom1Static.Room1CheckB++;
        }

        //LineC
        if (Mode == 3)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom1Static.Room1CheckC++;
        }

        //LineD
        if (Mode == 4)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane5")
                ElectricityPuzzleRoom1Static.Room1CheckD++;
        }

        //LineE
        if (Mode == 5)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane7")
                ElectricityPuzzleRoom1Static.Room1CheckE++;
        }

        //LineF
        if (Mode == 6)
        {
            if (other.gameObject.name == "ui_plane5" || other.gameObject.name == "ui_plane6")
                ElectricityPuzzleRoom1Static.Room1CheckF++;
        }

        //LineG
        if (Mode == 7)
        {
            if (other.gameObject.name == "ui_plane6" || other.gameObject.name == "ui_plane7")
                ElectricityPuzzleRoom1Static.Room1CheckG++;
        }

        //LineH
        if (Mode == 8)
        {
            if (other.gameObject.name == "ui_plane7" || other.gameObject.name == "ui_plane8")
                ElectricityPuzzleRoom1Static.Room1CheckH++;
        }


    }
    void OnTriggerExit(Collider other)
    {
        //LineA
        if (Mode == 1)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane2")
                ElectricityPuzzleRoom1Static.Room1CheckA--;
        }

        //LineB
        if (Mode == 2)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom1Static.Room1CheckB--;
        }

        //LineC
        if (Mode == 3)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom1Static.Room1CheckC--;
        }

        //LineD
        if (Mode == 4)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane5")
                ElectricityPuzzleRoom1Static.Room1CheckD--;
        }

        //LineE
        if (Mode == 5)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane7")
                ElectricityPuzzleRoom1Static.Room1CheckE--;
        }

        //LineF
        if (Mode == 6)
        {
            if (other.gameObject.name == "ui_plane5" || other.gameObject.name == "ui_plane6")
                ElectricityPuzzleRoom1Static.Room1CheckF--;
        }

        //LineG
        if (Mode == 7)
        {
            if (other.gameObject.name == "ui_plane6" || other.gameObject.name == "ui_plane7")
                ElectricityPuzzleRoom1Static.Room1CheckG--;
        }

        //LineH
        if (Mode == 8)
        {
            if (other.gameObject.name == "ui_plane7" || other.gameObject.name == "ui_plane8")
                ElectricityPuzzleRoom1Static.Room1CheckH--;
        }
    }
}

