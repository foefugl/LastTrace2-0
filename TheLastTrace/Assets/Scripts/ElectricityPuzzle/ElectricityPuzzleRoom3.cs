﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityPuzzleRoom3 : MonoBehaviour
{
    public float Mode = 0;
    public LineRenderer _LineRenderer;
    public Material RadLine;
    public Material GreenLine;

    public MeshRenderer Screen;
    public Material ScreenGreenMaterial;
    public Material ScreenRedMaterial;

    // Start is called before the first frame update

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (ElectricityPuzzleRoom3Static.RoomCheckA > 2)
            ElectricityPuzzleRoom3Static.RoomCheckA = 2;

        if (ElectricityPuzzleRoom3Static.RoomCheckB > 2)
            ElectricityPuzzleRoom3Static.RoomCheckB = 2;

        if (ElectricityPuzzleRoom3Static.RoomCheckC > 2)
            ElectricityPuzzleRoom3Static.RoomCheckC = 2;

        if (ElectricityPuzzleRoom3Static.RoomCheckD > 2)
            ElectricityPuzzleRoom3Static.RoomCheckD = 2;

        if (ElectricityPuzzleRoom3Static.RoomCheckE > 2)
            ElectricityPuzzleRoom3Static.RoomCheckE = 2;

        //CheckA
        if (Mode == 1)
        {
            if (ElectricityPuzzleRoom3Static.RoomCheckA == 2)
            {
                _LineRenderer.material = GreenLine;
                ElectricityPuzzleRoom3Static.RoomBoolA = true;
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom3Static.RoomBoolA = false;
            }
        }

        //CheckB
        if (Mode == 2)
        {
            if (ElectricityPuzzleRoom3Static.RoomBoolA == true)
            {
                if (ElectricityPuzzleRoom3Static.RoomCheckB == 2)
                {
                    _LineRenderer.material = GreenLine;
                    ElectricityPuzzleRoom3Static.RoomBoolB = true;
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom3Static.RoomBoolB = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom3Static.RoomBoolB = false;
            }
        }

        //CheckC
        if (Mode == 3)
        {
            if (ElectricityPuzzleRoom3Static.RoomBoolA == true)
            {
                if (ElectricityPuzzleRoom3Static.RoomCheckC == 2)
                {
                    _LineRenderer.material = GreenLine;
                    ElectricityPuzzleRoom3Static.RoomBoolC = true;
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom3Static.RoomBoolC = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom3Static.RoomBoolC = false;
            }
        }

        //CheckD
        if (Mode == 4)
        {
            if (ElectricityPuzzleRoom3Static.RoomBoolC == true || ElectricityPuzzleRoom3Static.RoomBoolE == true )
            {
                if (ElectricityPuzzleRoom3Static.RoomCheckD == 2)
                {
                    if (!ElectricityPuzzleRoom3Static.RoomBoolA)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom3Static.RoomBoolD = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom3Static.RoomBoolD = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom3Static.RoomBoolD = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom3Static.RoomBoolD = false;
            }

        }

        //CheckE
        if (Mode == 5)
        {
            if (ElectricityPuzzleRoom3Static.RoomBoolB == true || ElectricityPuzzleRoom3Static.RoomBoolD == true)
            {
                if (ElectricityPuzzleRoom3Static.RoomCheckE == 2)
                {
                    if (!ElectricityPuzzleRoom3Static.RoomBoolA)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom3Static.RoomBoolE = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom3Static.RoomBoolE = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom3Static.RoomBoolE = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom3Static.RoomBoolE = false;
            }

            if (ElectricityPuzzleRoom3Static.RoomBoolA == true && ElectricityPuzzleRoom3Static.RoomBoolB == true && ElectricityPuzzleRoom3Static.RoomBoolC == true && ElectricityPuzzleRoom3Static.RoomBoolD == true && ElectricityPuzzleRoom3Static.RoomBoolE == true)
            {
                Screen.material = ScreenGreenMaterial;
            }
            else
            {
                Screen.material = ScreenRedMaterial;
            }
        }





    }
    void OnTriggerEnter(Collider other)
    {
        //LineA
        if (Mode == 1)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane2")
                ElectricityPuzzleRoom3Static.RoomCheckA++;
        }

        //LineB
        if (Mode == 2)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom3Static.RoomCheckB++;
        }

        //LineC
        if (Mode == 3)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane5")
                ElectricityPuzzleRoom3Static.RoomCheckC++;
        }

        //LineD
        if (Mode == 4)
        {
            if (other.gameObject.name == "ui_plane5" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom3Static.RoomCheckD++;
        }

        //LineE
        if (Mode == 5)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom3Static.RoomCheckE++;
        }

    }
    void OnTriggerExit(Collider other)
    {
        //LineA
        if (Mode == 1)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane2")
                ElectricityPuzzleRoom3Static.RoomCheckA--;
        }

        //LineB
        if (Mode == 2)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom3Static.RoomCheckB--;
        }

        //LineC
        if (Mode == 3)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane5")
                ElectricityPuzzleRoom3Static.RoomCheckC--;
        }

        //LineD
        if (Mode == 4)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane5")
                ElectricityPuzzleRoom3Static.RoomCheckD--;
        }

        //LineE
        if (Mode == 5)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom3Static.RoomCheckE--;
        }

    }
}
