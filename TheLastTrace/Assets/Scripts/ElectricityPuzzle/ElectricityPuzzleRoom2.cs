﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityPuzzleRoom2 : MonoBehaviour
{
    public float Mode = 0;
    public LineRenderer _LineRenderer;
    public Material RadLine;
    public Material GreenLine;

    public MeshRenderer Screen;
    public Material ScreenGreenMaterial;
    public Material ScreenRedMaterial;

    // Start is called before the first frame update

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (ElectricityPuzzleRoom2Static.RoomCheckA > 2)
            ElectricityPuzzleRoom2Static.RoomCheckA = 2;

        if (ElectricityPuzzleRoom2Static.RoomCheckB > 2)
            ElectricityPuzzleRoom2Static.RoomCheckB = 2;

        if (ElectricityPuzzleRoom2Static.RoomCheckC > 2)
            ElectricityPuzzleRoom2Static.RoomCheckC = 2;

        if (ElectricityPuzzleRoom2Static.RoomCheckD > 2)
            ElectricityPuzzleRoom2Static.RoomCheckD = 2;

        if (ElectricityPuzzleRoom2Static.RoomCheckE > 2)
            ElectricityPuzzleRoom2Static.RoomCheckE = 2;

        if (ElectricityPuzzleRoom2Static.RoomCheckF > 2)
            ElectricityPuzzleRoom2Static.RoomCheckF = 2;

        if (ElectricityPuzzleRoom2Static.RoomCheckG > 2)
            ElectricityPuzzleRoom2Static.RoomCheckG = 2;
        //CheckA
        if (Mode == 1)
        {
            if (ElectricityPuzzleRoom2Static.RoomCheckA == 2)
            {
                _LineRenderer.material = GreenLine;
                ElectricityPuzzleRoom2Static.RoomBoolA = true;
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom2Static.RoomBoolA = false;
            }
        }

        //CheckB
        if (Mode == 2)
        {
            if (ElectricityPuzzleRoom2Static.RoomBoolA == true | ElectricityPuzzleRoom2Static.RoomBoolD == true | ElectricityPuzzleRoom2Static.RoomBoolC == true)
            {

                if (ElectricityPuzzleRoom2Static.RoomCheckB == 2)
                {
                    if (!ElectricityPuzzleRoom2Static.RoomBoolA && !ElectricityPuzzleRoom2Static.RoomBoolG)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom2Static.RoomBoolB = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom2Static.RoomBoolB = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom2Static.RoomBoolB = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom2Static.RoomBoolB = false;
            }
        }

        //CheckC
        if (Mode == 3)
        {
            if (ElectricityPuzzleRoom2Static.RoomBoolB == true | ElectricityPuzzleRoom2Static.RoomBoolD == true | ElectricityPuzzleRoom2Static.RoomBoolE == true)
            {
                if (ElectricityPuzzleRoom2Static.RoomCheckC == 2)
                {
                    if (!ElectricityPuzzleRoom2Static.RoomBoolA && !ElectricityPuzzleRoom2Static.RoomBoolG)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom2Static.RoomBoolC = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom2Static.RoomBoolC = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom2Static.RoomBoolC = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom2Static.RoomBoolC = false;
            }
        }

        //CheckD
        if (Mode == 4)
        {
            if (ElectricityPuzzleRoom2Static.RoomBoolB == true | ElectricityPuzzleRoom2Static.RoomBoolC == true | ElectricityPuzzleRoom2Static.RoomBoolG == true | ElectricityPuzzleRoom2Static.RoomBoolF == true)
            {
                if (ElectricityPuzzleRoom2Static.RoomCheckD == 2)
                {
                    if (!ElectricityPuzzleRoom2Static.RoomBoolA && !ElectricityPuzzleRoom2Static.RoomBoolG)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom2Static.RoomBoolD = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom2Static.RoomBoolD = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom2Static.RoomBoolD = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom2Static.RoomBoolD = false;
            }

        }

        //CheckE
        if (Mode == 5)
        {
            if (ElectricityPuzzleRoom2Static.RoomBoolC == true | ElectricityPuzzleRoom2Static.RoomBoolF == true)
            {
                if (ElectricityPuzzleRoom2Static.RoomCheckE == 2)
                {
                    if (!ElectricityPuzzleRoom2Static.RoomBoolA && !ElectricityPuzzleRoom2Static.RoomBoolG)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom2Static.RoomBoolE = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom2Static.RoomBoolE = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom2Static.RoomBoolE = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom2Static.RoomBoolE = false;
            }
        }

        //CheckF
        if (Mode == 6)
        {
            if (ElectricityPuzzleRoom2Static.RoomBoolD == true | ElectricityPuzzleRoom2Static.RoomBoolG == true | ElectricityPuzzleRoom2Static.RoomBoolE == true)
            {
                if (ElectricityPuzzleRoom2Static.RoomCheckF == 2)
                {
                    if (!ElectricityPuzzleRoom2Static.RoomBoolA && !ElectricityPuzzleRoom2Static.RoomBoolG)
                    {
                        _LineRenderer.material = RadLine;
                        ElectricityPuzzleRoom2Static.RoomBoolF = false;
                    }
                    else
                    {
                        _LineRenderer.material = GreenLine;
                        ElectricityPuzzleRoom2Static.RoomBoolF = true;
                    }
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom2Static.RoomBoolF = false;
                }
            }
            else
            {
                _LineRenderer.material = RadLine;
                ElectricityPuzzleRoom2Static.RoomBoolF = false;
            }
        }


        //CheckG
        if (Mode == 7)
        {

                if (ElectricityPuzzleRoom2Static.RoomCheckG == 2)
                {
                    _LineRenderer.material = GreenLine;
                    ElectricityPuzzleRoom2Static.RoomBoolG = true;
                }
                else
                {
                    _LineRenderer.material = RadLine;
                    ElectricityPuzzleRoom2Static.RoomBoolG = false;
                }
            


            if (ElectricityPuzzleRoom2Static.RoomBoolA == true && ElectricityPuzzleRoom2Static.RoomBoolB == true && ElectricityPuzzleRoom2Static.RoomBoolC == true && ElectricityPuzzleRoom2Static.RoomBoolD == true && ElectricityPuzzleRoom2Static.RoomBoolE == true && ElectricityPuzzleRoom2Static.RoomBoolF == true && ElectricityPuzzleRoom2Static.RoomBoolG == true)
            {
                Screen.material = ScreenGreenMaterial;
            }
            else
            {
                Screen.material = ScreenRedMaterial;
            }

        }

        


    }
    void OnTriggerEnter(Collider other)
    {
        //LineA
        if (Mode == 1)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane2")
                ElectricityPuzzleRoom2Static.RoomCheckA++;
        }

        //LineB
        if (Mode == 2)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom2Static.RoomCheckB++;
        }

        //LineC
        if (Mode == 3)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane6")
                ElectricityPuzzleRoom2Static.RoomCheckC++;
        }

        //LineD
        if (Mode == 4)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom2Static.RoomCheckD++;
        }

        //LineE
        if (Mode == 5)
        {
            if (other.gameObject.name == "ui_plane5" || other.gameObject.name == "ui_plane6")
                ElectricityPuzzleRoom2Static.RoomCheckE++;
        }

        //LineF
        if (Mode == 6)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane5")
                ElectricityPuzzleRoom2Static.RoomCheckF++;
        }

        //LineG
        if (Mode == 7)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom2Static.RoomCheckG++;
        }




    }
    void OnTriggerExit(Collider other)
    {
        //LineA
        if (Mode == 1)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane2")
                ElectricityPuzzleRoom2Static.RoomCheckA--;
        }

        //LineB
        if (Mode == 2)
        {
            if (other.gameObject.name == "ui_plane2" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom2Static.RoomCheckB--;
        }

        //LineC
        if (Mode == 3)
        {
            if (other.gameObject.name == "ui_plane3" || other.gameObject.name == "ui_plane6")
                ElectricityPuzzleRoom2Static.RoomCheckC--;
        }

        //LineD
        if (Mode == 4)
        {
            if (other.gameObject.name == "ui_plane4" || other.gameObject.name == "ui_plane3")
                ElectricityPuzzleRoom2Static.RoomCheckD--;
        }

        //LineE
        if (Mode == 5)
        {
            if (other.gameObject.name == "ui_plane5" || other.gameObject.name == "ui_plane6")
                ElectricityPuzzleRoom2Static.RoomCheckE--;
        }

        //LineF
        if (Mode == 6)
        {
            if (other.gameObject.name == "ui_plane5" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom2Static.RoomCheckF--;
        }

        //LineG
        if (Mode == 7)
        {
            if (other.gameObject.name == "ui_plane1" || other.gameObject.name == "ui_plane4")
                ElectricityPuzzleRoom2Static.RoomCheckG--;
        }


    }
}
