﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtUI : MonoBehaviour
{
    public Transform _PlayerTransform;
    //public Transform _GameObjectTransform;
    public float Speed=0.03f;
    public float Range=50;

    float A = 0;
    

    Vector2 pos1;
    Vector2 pos2;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        A++;
        if (A < Range)
        {
            //当达到最右边时的最远距离
            transform.position = new Vector3(transform.position.x, transform.position.y+ Speed * Time.deltaTime, transform.position.z);
        }

        //当达到最右边时的最远距离并且回到原点时
        if (A > Range && A < Range*2)
        {
            transform.position = new Vector3(transform.position.x , transform.position.y - Speed * Time.deltaTime, transform.position.z);
        }
        if (A > Range*2)
        {
            //原点继续出去
            A = 0;
        }


        pos1.x = _PlayerTransform.position.x;
        pos1.y = _PlayerTransform.position.z;
        //pos2.x = _GameObjectTransform.position.x;
        //pos2.y = _GameObjectTransform.position.z;

        this.transform.LookAt(Camera.main.transform.position);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(Camera.main.transform.position - this.transform.position), 0);
        

        
    }
}
