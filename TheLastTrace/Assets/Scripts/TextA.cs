﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextA : MonoBehaviour
{
    public Color Color;
    // Start is called before the first frame update
    void Start()
    {
        Color = GetComponent<Color>();
    }

    // Update is called once per frame
    void Update()
    {
        Color.a = Mathf.PingPong(20, 225);
    }
}
