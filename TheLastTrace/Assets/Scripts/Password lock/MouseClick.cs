﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : MonoBehaviour
{
    

    public int Mode = 1;
    public PasswordLock _PasswordLock;
    int I = 0;
    Animator _Animator;
    public AudioSource _AudioSource;
    public Collider _Collider;
    
    // Start is called before the first frame update
    void Start()
    {
        _Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (I > 9)
            I = 0;
        _Animator.SetInteger("Int", I);
    }
    void Rotate()
    {

        if (I < 10)
        {
            I += 1;

        }

        




    }
    public void Lock()
    {
        _Collider.enabled = false;
    }

    public void Open()
    {
        _Collider.enabled = true;
    }


    void OnMouseDown()
    {
        Lock();
        Invoke("Open", 0.3f);
        _AudioSource.Play();
        if (Mode == 1) 
        {
            Rotate();
            _PasswordLock.A += 1;
            if(_PasswordLock.A > 9)
            {
                _PasswordLock.A = 0;
            }
            _PasswordLock.Check();
        }
        if (Mode == 2)
        {
            Rotate();
            _PasswordLock.B += 1;
            if (_PasswordLock.B > 9)
            {
                _PasswordLock.B = 0;
            }
            _PasswordLock.Check();
        }
        if (Mode == 3)
        {
            Rotate();
            _PasswordLock.C += 1;
            if (_PasswordLock.C > 9)
            {
                _PasswordLock.C = 0;
            }
            _PasswordLock.Check();
        }
        if (Mode == 4)
        {
            Rotate();
            _PasswordLock.D += 1;
            if (_PasswordLock.D > 9)
            {
                _PasswordLock.D = 0;
            }
            _PasswordLock.Check();
        }
    }
}
