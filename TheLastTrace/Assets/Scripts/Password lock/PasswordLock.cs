﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasswordLock : MonoBehaviour
{
    public float _PasswordA;
    public float _PasswordB;
    public float _PasswordC;
    public float _PasswordD;

    public float A;
    public float B;
    public float C;
    public float D;

    public GameObject ClickESC;
    public Animator _Animator;
    public Hitter _Hitter;
    public RayPlay _RayPlay;
    public AudioSource _AudioSource;
    public GameObject LabLockUI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Check()
    {
        if(_PasswordA == A && _PasswordB == B && _PasswordC == C&& _PasswordD == D)
        {
            ClickESC.SetActive(false);
            _Hitter.ZoomCh = 2;
            _Animator.SetTrigger("Open");
            _RayPlay.enabled = true;
            AlbumStatic.PassLock = true;
            _AudioSource.Play();
            if(AlbumStatic.Lab)
            {
                LabLockUI.SetActive(false);
            }
        }

    }
}
