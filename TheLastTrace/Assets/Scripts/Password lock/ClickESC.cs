﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClickESC : MonoBehaviour
{
    GameObject _GameObject;
    public Hitter _Hitter;
    public Collider PasswordlockCollider;
    public RayPlay _RayPlay;
    // Start is called before the first frame update
    void Start()
    {
        _GameObject = this.gameObject;
        _GameObject.SetActive(false);

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnMouseDown()
    {
        Invoke("_PasswordlockCollider", 3);
        _GameObject.SetActive(false);
        _RayPlay.enabled = true;
        
        _Hitter.ZoomCh = 2;
        
    }
    void _PasswordlockCollider()
    {
        PasswordlockCollider.enabled = true;
    }
}
