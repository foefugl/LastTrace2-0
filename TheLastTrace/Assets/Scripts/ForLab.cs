﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ForLab : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        AlbumStatic.Lab = true;
        AlbumStatic.RoomCMode6 = false;
        AlbumStatic.RoomCMode7 = true;
        AlbumStatic.Album = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            SceneManager.LoadScene(3);
        }
    }
}
