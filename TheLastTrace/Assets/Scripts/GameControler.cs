﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControler : MonoBehaviour
{
    public ChangeWorld _ChangeWorld;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            _ChangeWorld.ChangeHouseA();

        if (Input.GetKeyDown(KeyCode.Alpha2))
            _ChangeWorld.ChangeHouseB();

        if (Input.GetKeyDown(KeyCode.Alpha3))
            _ChangeWorld.ChangeHouseC();
    }
}
