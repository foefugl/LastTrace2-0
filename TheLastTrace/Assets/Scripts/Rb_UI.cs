﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rb_UI : MonoBehaviour
{
    public GameObject PutDownUI;
    public GameObject FinalUI;
    // Start is called before the first frame update
    void Start()
    {
        FinalUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(StaticNum.LiftUprb_hand02==true|| StaticNum.LiftUprb_hand02_1 == true|| StaticNum.LiftUprb_hand02_2 == true|| StaticNum.LiftUprb_hand03 == true|| StaticNum.LiftUprb_hand04 == true|| StaticNum.LiftUprb_hand05 == true|| StaticNum.LiftUprb_hand06 == true)
        {
            PutDownUI.SetActive(true);
            if(StaticNum.rb_handMode == 1&&StaticNum.LiftUprb_hand02 == true)
            {
                FinalUI.SetActive(true);
            }
            else if (StaticNum.rb_handMode == 2 && StaticNum.LiftUprb_hand02_1 == true)
            {
                FinalUI.SetActive(true);
            }
            else if (StaticNum.rb_handMode == 3 && StaticNum.LiftUprb_hand02_2 == true)
            {
                FinalUI.SetActive(true);
            }
            else if (StaticNum.rb_handMode == 4 && StaticNum.LiftUprb_hand03 == true)
            {
                FinalUI.SetActive(true);
            }
            else if (StaticNum.rb_handMode == 5 && StaticNum.LiftUprb_hand04 == true)
            {
                FinalUI.SetActive(true);
            }
            else if (StaticNum.rb_handMode == 6 && StaticNum.LiftUprb_hand05 == true)
            {
                FinalUI.SetActive(true);
            }
            else if (StaticNum.rb_handMode == 7 && StaticNum.LiftUprb_hand06 == true)
            {
                FinalUI.SetActive(true);
            }
            else
            {
                FinalUI.SetActive(false);
            }
        }
        else
        {
            PutDownUI.SetActive(false);
            FinalUI.SetActive(false);
        }









    }
}
