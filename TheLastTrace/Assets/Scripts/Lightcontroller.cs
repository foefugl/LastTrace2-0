﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;

public class Lightcontroller : MonoBehaviour
{
    [Header("控制大廳及警衛室的光")]

    public Light[] _Light;

    [ColorUsage(true, true)]
    private Color matColor;


    // Start is called before the first frame update

    void Start()
    {
        for (int i = 0; i < _Light.Length + 1; i++)
        {
            Renderer nRend = _Light[i].GetComponent<Renderer>();
            _Light[i].color = matColor;

        }

        for (int i = 0; i < _Light.Length + 1; i++)
        {
            _Light[i].intensity = 0;
        }
        
    }

    // Update is called once per frame
    void Update()
    {

        if(StaticNum.Lightcontroller)
        {
            for (int i = 0; i < _Light.Length+1; i++)
            {
                _Light[i].intensity = 0;
            }
        }
        if (!StaticNum.Lightcontroller)
        {
            for (int i = 0; i < _Light.Length + 1; i++)
            {
                _Light[i].intensity = 4;
            }
        }
    }

}
