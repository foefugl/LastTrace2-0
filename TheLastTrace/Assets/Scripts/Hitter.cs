﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class Hitter : MonoBehaviour
{
    public GameObject _GameObject;
    [Header("音效組件")]
    public AudioSource _AudioSource;

    [Header("鏡頭ZOOM IN組件")]
    public Camera _Camera;
    public GameObject _CameraMoveTo;
    public GameObject SavePoint;
    public float CameraMoveSpeed = 0.1f;
    public bool IsZoomIn = true;
    public float ZoomCh = 0;
    public float TimeC;
    public FirstPersonAIO _FirstPersonAIO;

    [Header("開門組件")]
    public Animator _DoorAnimator;
    bool IsOpenDoor=false;
    public GameObject UI;

    [Header("電閘開關組件")]
    public NoPowerBigDoorController _NoPowerBigDoorController;
    public Material ServerOff;
    public Animator _SwitchAnimator;
    public GameObject PowerUI;
   public Animator _playerCameraAnimator;

    [Header("密碼鎖組件")]
    public Collider PasswordlockCollider;
    public GameObject Card;
    public GameObject ClickESC;
    public RayPlay _RayPlay;
    public GameObject LockUI;

    [Header("電路解謎組件")]
    public GameObject Player;
    public GameObject LineGame;
    public bool InLineGame = false;
    public bool CanPlay=true;
    public GameObject ComputerUI;
    public GameObject AllUI;


    [Header("抬起物品組件")]
    public GameObject LiftedGameObject;
    public GameObject Weapon;

    [Header("組合機械手臂組件")]
    public GameObject rb_hand_finish;
    public GameObject rb_hand_finish01;
    public GameObject rb_hand_finish02;
    public GameObject rb_hand_finish02_1;
    public GameObject rb_hand_finish02_2;
    public GameObject rb_hand_finish03;
    public GameObject rb_hand_finish04;
    public GameObject rb_hand_finish05;
    public GameObject rb_hand_finish06;
    public GameObject On_hand_finish01;
    public GameObject On_hand_finish02;
    public GameObject On_hand_finish02_1;
    public GameObject On_hand_finish02_2;
    public GameObject On_hand_finish03;
    public GameObject On_hand_finish04;
    public GameObject On_hand_finish05;
    public GameObject On_hand_finish06;
    public PlayableDirector director;
    public GameObject Direct;


    [Header("放下機械手臂組件")]
    public Collider PutDownComponentsCollider;
    public GameObject PutDown01;
    public GameObject PutDown02;
    public GameObject PutDown02_1;
    public GameObject PutDown02_2;
    public GameObject PutDown03;
    public GameObject PutDown04;
    public GameObject PutDown05;
    public GameObject PutDown06;

    [Header("全息投影組件")]
    public Animator bottom_projector;
    public bool In = false;

    [Header("書櫃移動組件")]
    public Animator past_shelfAnimator;

    [Header("搬運箱子組件")]
    public GameObject ModelOnHand;
    public GameObject ModelOnTable;

    [Header("開陽台門移動組件")]
    public Animator Balconydoor;

    [Header("花盆移動組件")]
    public Animator Vaso_A002;

    [Header("開暗房門移動組件")]
    public Animator BlackRoomDoor;

    [Header("便條紙組件組件")]
    public GameObject RoomBdirectorOBJ;
    public PlayableDirector RoomBdirector;

    [Header("玫瑰花組件")]
    public GameObject RoseUI;
    public GameObject RoseOnHand;
    public GameObject PutDownRose;
    public GameObject PutDownRoseUI;
    public PlayableDirector Enddirector;
    public GameObject EnddirectorOBJ;


    /* public GameObject _ChangeWorld;
     public Animator _ChangeWorldAnimator;
     public GameObject _WeaponSystemobj;
     public WeaponSystem _WeaponSystem;
     public LockController _LockController;
     public CameraController _CameraController;
     public BookPage _BookPage;
     public GameObject _BookroomBooks;
     public GameObject _RayController;  
     public GameObject _AudioController;
     public AudioSourceController _AudioSourceController;
     public Animator _bookroomdoor;



     public GameObject _Box;
     public GameObject _PutBox;

     public bool _CanTakeBook = false;*/

    bool IsIN = false;
    float speed = 0.1f;
    float tmp = 20;

    public void ChangeTOMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ChangeHouseC()
    {
        SceneManager.LoadScene(3);

    }

    void Start()
    {
        _GameObject = this.gameObject;
        
        /*_ChangeWorld = GameObject.Find("ChangeWorld");
        _ChangeWorldAnimator = _ChangeWorld.GetComponent<Animator>();
        _WeaponSystemobj = GameObject.Find("Weapon");
        _WeaponSystem = _WeaponSystemobj.GetComponent<WeaponSystem>();
        
        _AudioController = GameObject.Find("AudioController");
        _AudioSourceController = _AudioController.GetComponent<AudioSourceController>();*/

        
    }

    // Update is called once per frame
    void Update()
    {
        if (_GameObject.name == "Counter_PC_Screen")
        {
            if (!IsZoomIn && !InLineGame)
            {
                Player.SetActive(false);
                LineGame.SetActive(true);
            }

            if (ZoomCh == 2 && InLineGame)
            {
                Player.SetActive(true);
                LineGame.SetActive(false);
            }
        }


        if (ZoomCh == 1)
        {
            _FirstPersonAIO.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            ZoomIn();
        }

        if (ZoomCh == 2)
        {
            ZoomOut();
        }
    }

    void ZoomIn()
    {
        TimeC += Time.deltaTime;

        _Camera.transform.position = Vector3.Lerp(_Camera.transform.position, _CameraMoveTo.transform.position, CameraMoveSpeed);
        _Camera.transform.rotation = Quaternion.Lerp(_Camera.transform.rotation, _CameraMoveTo.transform.rotation, CameraMoveSpeed);
        
        if (TimeC > 2f)
        {
            IsZoomIn = false;
            ZoomCh = 0;
            Debug.Log(1);
            TimeC = 0;
        }
    }
    void ZoomOut()
    {
        TimeC += Time.deltaTime;
        _Camera.transform.position = Vector3.Lerp(_Camera.transform.position, SavePoint.transform.position, CameraMoveSpeed);
        _Camera.transform.rotation = Quaternion.Lerp(_Camera.transform.rotation, SavePoint.transform.rotation, CameraMoveSpeed);

        if (TimeC > 2f)
        {

            _FirstPersonAIO.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            IsZoomIn = true;
            ZoomCh = 0;
            TimeC = 0;

        }
    }

    void HitByRaycast() //被射線打到時會進入此方法
    {
        //撿玫瑰花
        if(_GameObject.name=="rose")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _GameObject.SetActive(false);
                RoseUI.SetActive(false);
                RoseOnHand.SetActive(true);
                AlbumStatic.RoseOnHand = true;
                PutDownRoseUI.SetActive(true);
                _AudioSource.Play();

            }
        }

        if(_GameObject.name== "PutDownRose"&& AlbumStatic.RoseOnHand)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RoseOnHand.SetActive(false);
                AlbumStatic.RoseOnHand = false;
                PutDownRose.SetActive(true);
                PutDownRoseUI.SetActive(false);
                _AudioSource.Play();
                EnddirectorOBJ.SetActive(true);
                Enddirector.Play();
                Invoke("ChangeTOMenu", 11.5f);
            }
        }



        //實驗室開門
        if(_GameObject.name== "lab_small_door"||_GameObject.name== "glass")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (!IsOpenDoor)
                {
                    _DoorAnimator.SetTrigger("Open");
                    IsOpenDoor = true;
                }
            }
        }

        //電閘開關
        if (_GameObject.name == "MainPowerSwitch")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                PowerUI.SetActive(false);
                _playerCameraAnimator.SetTrigger("Open");
                _SwitchAnimator.SetTrigger("Open");
                _NoPowerBigDoorController.Screen.material = ServerOff;
                _NoPowerBigDoorController.HavePower = true;
                StaticNum.Lightcontroller = false;
            }
        }

        //取得ID卡
        if (_GameObject.name == "card")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                StaticNum.TakeIDCard = true;
                Destroy(this.gameObject);
                _AudioSource.Play();
            }
        }

        //開置物櫃
        if(_GameObject.name== "L_cabient_open_door")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _DoorAnimator.SetTrigger("Open");
                UI.SetActive(false);
            }
        }

        //取得記憶卡
        if (_GameObject.name == "ReadCard")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                StaticNum.TakeReadCard = true;
                StaticNum.ReadCard = true;
                Destroy(this.gameObject);
                _AudioSource.Play();
            }
        }

        //記憶卡插入投影機
        if(_GameObject.name== "Wall_part008")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (StaticNum.TakeReadCard&&!In)
                {
                    StaticNum.TakeReadCard = false;
                    StaticNum.ReadCard = true;
                    In = true;
                    bottom_projector.SetTrigger("In");
                    _AudioSource.Play();
                }
                else if(!StaticNum.TakeReadCard && In)
                {
                    bottom_projector.SetTrigger("Open");
                    _AudioSource.Play();
                }
            }
        }

        //ZOOM IN 到密碼鎖
        if (_GameObject.name == "lock")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Invoke("CLICKESC", 3);
                PasswordlockCollider.enabled = false;
                ZoomCh = 1;
                _RayPlay.enabled = false;
                SavePoint.transform.rotation = _Camera.transform.rotation;
            }
        }

        //把機器手臂零件抬起(會用到抬起與放下物品PutDownComponentsCollider組件)
        if (_GameObject.name== "rb_hand01"|| _GameObject.name == "rb_hand02"|| _GameObject.name == "rb_hand02-1"|| _GameObject.name == "rb_hand02-2"|| _GameObject.name == "rb_hand03"|| _GameObject.name == "rb_hand04"|| _GameObject.name == "rb_hand05"|| _GameObject.name == "rb_hand06")
        {
            if (!StaticNum.SomeThingLifted)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    PutDownComponentsCollider.enabled = true;
                    StaticNum.SomeThingLifted = true;
                    Weapon.SetActive(false);
                    _GameObject.SetActive(false);
                    LiftedGameObject.SetActive(true);
                    if(_GameObject.name == "rb_hand01")
                    {
                        StaticNum.LiftUprb_hand01 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand02")
                    {
                        StaticNum.LiftUprb_hand02 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand02-1")
                    {
                        StaticNum.LiftUprb_hand02_1 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand02-2")
                    {
                        StaticNum.LiftUprb_hand02_2 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand03")
                    {
                        StaticNum.LiftUprb_hand03 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand04")
                    {
                        StaticNum.LiftUprb_hand04 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand05")
                    {
                        StaticNum.LiftUprb_hand05 = true;
                        _AudioSource.Play();
                    }
                    if (_GameObject.name == "rb_hand06")
                    {
                        StaticNum.LiftUprb_hand06 = true;
                        _AudioSource.Play();
                    }


                }
            }
        }

        //組合機器手臂(會用到放下物品PutDownComponentsCollider組件)
        if (_GameObject.name== "rb_hand_finish")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                /*if (StaticNum.rb_handMode == 1&& StaticNum.LiftUprb_hand01)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand01 = false;
                    On_hand_finish01.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish01.SetActive(true);
                    StaticNum.rb_handMode++;
                }*/
                if (StaticNum.rb_handMode == 1&& StaticNum.LiftUprb_hand02)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand02 = false;
                    On_hand_finish02.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish02.SetActive(true);
                    StaticNum.rb_handMode++;
                    _AudioSource.Play();
                }
                if (StaticNum.rb_handMode == 2&& StaticNum.LiftUprb_hand02_1)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand02_1 = false;
                    On_hand_finish02_1.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish02_1.SetActive(true);
                    StaticNum.rb_handMode++;
                    _AudioSource.Play();
                }
                if (StaticNum.rb_handMode == 3&& StaticNum.LiftUprb_hand02_2)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand02_2 = false;
                    On_hand_finish02_2.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish02_2.SetActive(true);
                    StaticNum.rb_handMode++;
                    _AudioSource.Play();
                }
                if (StaticNum.rb_handMode == 4&& StaticNum.LiftUprb_hand03)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand03 = false;
                    On_hand_finish03.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish03.SetActive(true);
                    StaticNum.rb_handMode++;
                    _AudioSource.Play();
                }
                if (StaticNum.rb_handMode == 5&& StaticNum.LiftUprb_hand04)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand04 = false;
                    On_hand_finish04.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish04.SetActive(true);
                    StaticNum.rb_handMode++;
                    _AudioSource.Play();
                }
                if (StaticNum.rb_handMode == 6&& StaticNum.LiftUprb_hand05)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand05 = false;
                    On_hand_finish05.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish05.SetActive(true);
                    StaticNum.rb_handMode++;
                    _AudioSource.Play();
                }
                if (StaticNum.rb_handMode == 7&& StaticNum.LiftUprb_hand06)
                {
                    PutDownComponentsCollider.enabled = false;
                    StaticNum.LiftUprb_hand06 = false;
                    On_hand_finish06.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    rb_hand_finish06.SetActive(true);
                    StaticNum.rb_handMode++;
                    rb_hand_finish.SetActive(false);
                    Direct.SetActive(true);
                    director.Play();
                    _AudioSource.Play();
                    Invoke("ChangeHouseC",30);
                }

            }
        }

        //放下機器手臂(會用到組合機械手臂On_hand組件)
        if (_GameObject.name== "PutDown_rb_Components")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (StaticNum.LiftUprb_hand01)
                {
                    
                    StaticNum.LiftUprb_hand01 = false;
                    On_hand_finish01.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown01.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand02)
                {
                    StaticNum.LiftUprb_hand02 = false;
                    On_hand_finish02.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown02.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand02_1)
                {
                    StaticNum.LiftUprb_hand02_1 = false;
                    On_hand_finish02_1.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown02_1.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand02_2)
                {
                    StaticNum.LiftUprb_hand02_2 = false;
                    On_hand_finish02_2.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown02_2.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand03)
                {
                    StaticNum.LiftUprb_hand03 = false;
                    On_hand_finish03.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown03.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand04)
                {
                    StaticNum.LiftUprb_hand04 = false;
                    On_hand_finish04.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown04.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand05)
                {
                    StaticNum.LiftUprb_hand05 = false;
                    On_hand_finish05.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown05.SetActive(true);
                    _AudioSource.Play();
                }
                if (StaticNum.LiftUprb_hand06)
                {
                    StaticNum.LiftUprb_hand06 = false;
                    On_hand_finish06.SetActive(false);
                    StaticNum.SomeThingLifted = false;
                    PutDownComponentsCollider.enabled = false;
                    PutDown06.SetActive(true);
                    _AudioSource.Play();
                }
            }
        }

        //進入電流解謎(會用到ZOOM IN組件)
        if(_GameObject.name== "Counter_PC_Screen"&& CanPlay)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                ZoomCh = 1;
                SavePoint.transform.rotation = _Camera.transform.rotation;
                ComputerUI.SetActive(false);
                AllUI.SetActive(false);
            }
        }

        //推動書櫃
        if (_GameObject.name == "past_shelf NeedMove")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0)&&AlbumStatic.RoomAMode2)
            {
                past_shelfAnimator.SetTrigger("Move");
                AlbumStatic.RoomCMode2 = false;
                AlbumStatic.RoomCMode3 = true;
                AlbumStatic.BookShelf = true;
            }
        }

        //撿起照片2
        if (_GameObject.name == "photo02")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _GameObject.SetActive(false);
                AlbumStatic.photo1 = false;
                AlbumStatic.photo2 = true;
                AlbumStatic.RoomAMode2 = false;
                AlbumStatic.RoomAMode3 = true;
                AlbumStatic.ReadingRoom = true;
                _AudioSource.Play();
            }
        }
        //撿起照片3
        if (_GameObject.name == "photo03")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _GameObject.SetActive(false);
                AlbumStatic.photo2 = false;
                AlbumStatic.photo3 = true;
                AlbumStatic.RoomAMode3 = false;
                AlbumStatic.RoomAMode4 = true;
                _AudioSource.Play();
            }
        }
        //撿起照片4
        if (_GameObject.name == "photo04")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _GameObject.SetActive(false);
                AlbumStatic.photo3 = false;
                AlbumStatic.photo4 = true;
                _AudioSource.Play();
            }
        }
        //撿起照片5
        if (_GameObject.name == "photo05")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _GameObject.SetActive(false);
                AlbumStatic.photo4 = false;
                AlbumStatic.photo5 = true;
                _AudioSource.Play();
            }
        }




        //搬箱子
        if (_GameObject.name == "model")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0)&& AlbumStatic.RoomAMode3)
            {
                _GameObject.SetActive(false);
                ModelOnHand.SetActive(true);
                AlbumStatic.Album = false;
                AlbumStatic.ModelIsOnHand = true;
                _AudioSource.Play();
            }
        }

        //放下箱子
        if (_GameObject.name == "PutModelPoint")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0)&& AlbumStatic.ModelIsOnHand)
            {
                ModelOnTable.SetActive(true);
                ModelOnHand.SetActive(false);
                AlbumStatic.Album = true;
                AlbumStatic.ModelIsOnHand = false;
                AlbumStatic.ModelIsOnTable = true;
                AlbumStatic.RoomCMode3 = false;
                AlbumStatic.RoomCMode4 = true;
                _AudioSource.Play();
            }
        }
        
        //開陽台門
        if (_GameObject.name == "Balconydoor"|| _GameObject.name == "doorknob_hand"||_GameObject.name== "door_glass")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && AlbumStatic.RoomAMode4)
            {
                AlbumStatic.Balconydoor = true;
                Balconydoor.SetTrigger("Open");
                AlbumStatic.RoomCMode5 = true;
                AlbumStatic.RoomCMode4 = false;

            }
        }

        //移動花盆
        if(_GameObject.name=="Vaso_A002"&&AlbumStatic.RoomAMode5)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Vaso_A002.SetTrigger("Move");
                AlbumStatic.Flower = true;
                AlbumStatic.RoomCMode6 = true;
                AlbumStatic.RoomCMode5 = false;
                _AudioSource.Play();
            }
        }

        //撿取暗房鑰匙
        if (_GameObject.name == "BlackRoomKey")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _GameObject.SetActive(false);
                AlbumStatic.BlackRoomKey = true;
                _AudioSource.Play();
            }
        }

        //開暗房門
        if (_GameObject.name == "BlackRoomDoor" || _GameObject.name == "BlackRoom_hand")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0)&&AlbumStatic.BlackRoomKey)
            {
                AlbumStatic.BlackRoomDoor = true;
                BlackRoomDoor.SetTrigger("Open");
                _AudioSource.Play();
            }
        }

        //開倉庫門
        if (_GameObject.name == "warehouseDoor")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _DoorAnimator.SetTrigger("Open");
                _AudioSource.Play();
            }
        }
        //開廁所門
        if (_GameObject.name == "Toiletdoor")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _DoorAnimator.SetTrigger("Open");
                _AudioSource.Play();
            }
        }

        //播放 TimeB TimeLine
        if(_GameObject.name== "08_kit_note02")
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RoomBdirectorOBJ.SetActive(true);
                RoomBdirector.Play();
                UI.SetActive(false);
            }
        }
        



        /*
         if (_GameObject.name == "ALBUM" && _CanTakeBook == false)
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _AudioSourceController.ChangeWorld.Play();
                 _AudioSourceController.World1BackMusic.Play();
                 _AudioSourceController.NowBackMusic.Stop();
                 _ChangeWorldAnimator.SetTrigger("OutWorld1");                
                 _CanTakeBook = true;

             }

         }
         else if (_GameObject.name == "ALBUM" && _CanTakeBook == true)
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _LockController.BookUIOn();
                 Destroy(this.gameObject);
             }
         }

         if (_GameObject.name == "album_past") 
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _AudioSourceController.ChangeWorld.Play();
                 _AudioSourceController.NowBackMusic.Play();
                 _AudioSourceController.World1BackMusic.Stop();
                 _ChangeWorldAnimator.SetTrigger("BackNow");
                 _LockController.past_shelf_on();
                 _bookroomdoor.SetTrigger("Opendoor");
                 Destroy(this.gameObject);
             }

         }

         if (_GameObject.name == "key") 
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 //_WeaponSystem._GetBKey();
                 _LockController.bookroom_door_on();
                 Destroy(this.gameObject);
             }

         }

         if (_GameObject.name == "Box1")
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _Box.SetActive(true);               
                 Destroy(this.gameObject);
             }

         }

         if (_GameObject.name == "PutBoxPoint")
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _bookroomdoor.SetTrigger("Opendoor");
                 _BookroomBooks.SetActive(true);
                 _Box.SetActive(false);
                 _PutBox.SetActive(true);
             }

         }

         if (_GameObject.name == "door")
         {

             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _AudioSourceController.DoorOpen.Play();
                 _bookroomdoor.SetTrigger("Opendoor");
                 _LockController.box_on();
             }

         }

         if (_GameObject.name == "photo01")
         {
             if (Input.GetKeyDown(KeyCode.Mouse0))
             {
                 _AudioSourceController.PaperFlipPlay();
                 _CameraController.Player.SetActive(false);
                 _CameraController.PlayerSetactive = false;
                 _BookPage.BookCanvas.alpha = 0;
                 _BookPage.WorldTwoPhoto.SetActive(true);
                 Destroy(this.gameObject);
             }

         }
         */


    }

    public void CLICKESC()
    {
        ClickESC.SetActive(true);
    }

    /*
     public void RayHitter()
     {
         _UIRay.Hand.SetActive(true);
         _UIRay.showImage();
         _UIRay.HaveImage = true;
         _UIRay.Target = this.gameObject;
     }
     */
}
