﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigDoorController : MonoBehaviour
{
    //有電大門控制
    Animator _BigDoor;
    public AudioSource _AudioSource;
    // Start is called before the first frame update
    void Start()
    {
        _BigDoor=GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {       
        _BigDoor.SetTrigger("Open");
        _AudioSource.Play();
    }
    private void OnTriggerExit(Collider other)
    {
        _BigDoor.SetTrigger("Close");
    }
}
