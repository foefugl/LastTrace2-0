﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBool : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;

    StaticNum.PowerUI = false;
    StaticNum.IdCardDoorLock = false;    
    StaticNum.BookIsTake = false;
    StaticNum.BookcaseIsMove = false;
    StaticNum.Lightcontroller = true;    
    StaticNum.TakeIDCard = false;        
    StaticNum.TakeReadCard = false;      
    StaticNum.ReadCard = false;          
    StaticNum.SomeThingLifted = false;      
    StaticNum.LiftUprb_hand01 = false;
    StaticNum.LiftUprb_hand01_2 = false;
    StaticNum.LiftUprb_hand02 = false;
    StaticNum.LiftUprb_hand02_1 = false;
    StaticNum.LiftUprb_hand02_2 = false;
    StaticNum.LiftUprb_hand03 = false;
    StaticNum.LiftUprb_hand04 = false;
    StaticNum.LiftUprb_hand05 = false;
    StaticNum.LiftUprb_hand06 = false;
    StaticNum.rb_handMode = 1;


    //玫瑰花是否拿在手中
    AlbumStatic.RoseOnHand = false;

    //TimeA的TimeLine是否撥放
    AlbumStatic.paperbox_director = false;
    AlbumStatic.balcony_director = false;

    //提示是否關閉
    AlbumStatic.Text = false;

    //照片是否撿取
    AlbumStatic.photo1 = false;
    AlbumStatic.photo2 = false;
    AlbumStatic.photo3 = false;
    AlbumStatic.photo4 = false;
    AlbumStatic.photo5 = false;

    //音效是否撥放
    AlbumStatic.ShelfAudioSource = false;
    AlbumStatic.FlowerAudioSource = false;


    //是否撿取暗房鑰匙
    AlbumStatic.BlackRoomKey = false;
    AlbumStatic.BlackRoomDoor = false;

    //密碼鎖是否被打開
    AlbumStatic.PassLock = false;

    //花盆是否移動
    AlbumStatic.Flower = false;

    //相簿是否撿取
    AlbumStatic.Album = false;

    //鑰匙是否撿起
    AlbumStatic.Key = false;

    //書櫃是否移動
    AlbumStatic.BookShelf = false;

    //書房門是否打開
    AlbumStatic.ReadingRoom = false;

    //書房玻璃門是否打開
    AlbumStatic.Balconydoor = false;

    //是否有拿起箱子
    AlbumStatic.ModelIsOnHand = false;

    //箱子是否放在桌上
    AlbumStatic.ModelIsOnTable = false;


    //RoomA的時間模式
    AlbumStatic.RoomAMode1 = true;
    AlbumStatic.RoomAMode2 = false;
    AlbumStatic.RoomAMode3 = false;
    AlbumStatic.RoomAMode4 = false;
    AlbumStatic.RoomAMode5 = false;

    
    AlbumStatic.RoomBMode1 = true;
    AlbumStatic.RoomBMode2 = false;
    AlbumStatic.RoomBMode3 = false;

    
    AlbumStatic.RoomCMode1 = true;
    AlbumStatic.RoomCMode2 = false;
    AlbumStatic.RoomCMode3 = false;
    AlbumStatic.RoomCMode4 = false;
    AlbumStatic.RoomCMode5 = false;
    AlbumStatic.RoomCMode6 = false;
    AlbumStatic.RoomCMode7 = false;

    AlbumStatic.Lab = false;


}

    // Update is called once per frame
    void Update()
    {
        
    }
}
