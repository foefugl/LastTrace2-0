﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoPowerBigDoorController : MonoBehaviour
{
    //沒電大門控制
    Animator _BigDoor;
    public Renderer Screen;
    public Material PowerOff;
    public Material PowerOffStop;
    public Material ServerOff;
    public Material ServerOffStop;
    public Material CanGo;

    public AudioSource lab_low_battery;
    public AudioSource lab_server_disconnecting;
    public AudioSource lab_big_door_open;
    public bool HavePower=false;
    public bool LineOn = false;
    void Start()
    {
        _BigDoor = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (!HavePower && !LineOn)
        {
            Screen.material = PowerOff;
        }
        else if (HavePower && !LineOn)
        {
            Screen.material = ServerOff;
        }
        else if(HavePower && LineOn)
        {
            Screen.material = CanGo;
        }*/
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!HavePower&&!LineOn)
        {
            Screen.material = PowerOffStop;
            lab_low_battery.Play();
        }
        else if(HavePower&&!LineOn)
        {
            Screen.material = ServerOffStop;
            lab_server_disconnecting.Play();
        }
        else if (HavePower && LineOn)
        {
            Screen.material = CanGo;
            _BigDoor.SetTrigger("Open");
            lab_big_door_open.Play();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!HavePower && !LineOn)
        {
            Screen.material = PowerOff;
        }
        else if (HavePower && !LineOn)
        {
            Screen.material = ServerOff;
        }
        else if (HavePower && LineOn)
        {
            Screen.material = CanGo;
            _BigDoor.SetTrigger("Close");
        }
    }
}
