﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayPlay : MonoBehaviour
{
    Ray _Ray;
    public float _MaxRay;
    RaycastHit _Hit;
    public LayerMask _LayerMask;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {



        _Ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        //由攝影機射到是畫面正中央的射線

        if (Physics.Raycast(_Ray, out _Hit, _MaxRay, _LayerMask))
        // (射線,out 被射線打到的物件,射線長度)，out hit 意思是：把"被射線打到的物件"帶給hit
        {
            _Hit.transform.SendMessage("HitByRaycast", gameObject, SendMessageOptions.DontRequireReceiver);
            //向被射線打到的物件呼叫名為"HitByRaycast"的方法，不需要傳回覆

            Debug.DrawLine(_Ray.origin, _Hit.point, Color.yellow);
            //當射線打到物件時會在Scene視窗畫出黃線

            print(_Hit.transform.name);
            //在Console視窗印出被射線打到的物件名稱                   
        }
        else
        {

        }
    }



    void HitByRaycast() //被射線打到時會進入此方法
    {
        if (Input.GetButtonUp("E")) //當按下鍵盤 E 鍵時
        {

        }
        else
        {

        }
        
    }
}
