﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class line_position_cotroller : MonoBehaviour
{
    public LineRenderer lr;
    public GameObject point01;
    public GameObject point02;
        
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lr.SetPosition(0, point01.transform.position);
        lr.SetPosition(1, point02.transform.position);
    }
}
