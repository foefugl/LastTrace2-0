﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_MatController : MonoBehaviour
{

    public float all_light_intensity;
    public float warning_intensity;
    public float noise_volume;
    public Material panel;
    public Vector2 bar1;
    public Vector2 bar2;
    public Vector2 bar3;
    public Vector2 bar4;
    public Vector2 bar5;
    public Vector2 power_volume;
    public Vector2 tamperature_volume;
    

    void Start()
    {
     
        panel.SetFloat("_all", all_light_intensity);
        panel.SetFloat("_war_intensity", warning_intensity);
        panel.SetFloat("_noi", noise_volume);
        panel.SetVector("_power", power_volume);
        panel.SetVector( "_temperature", tamperature_volume);
        panel.SetVector("_bar1", bar1);
        panel.SetVector("_bar2", bar2);
        panel.SetVector("_bar3", bar3);
        panel.SetVector("_bar4", bar4);
        panel.SetVector("_bar5", bar5);

    }

    // Update is called once per frame
    void Update()
    {


        panel.SetFloat("_all", all_light_intensity);
        panel.SetFloat("_war_intensity", warning_intensity);
        panel.SetFloat("_noi", noise_volume);
        panel.SetVector("_power", power_volume);
        panel.SetVector("_temperature", tamperature_volume);
        panel.SetVector("_bar1", bar1);
        panel.SetVector("_bar2", bar2);
        panel.SetVector("_bar3", bar3);
        panel.SetVector("_bar4", bar4);
        panel.SetVector("_bar5", bar5);
    }
}
